RELEASE NOTES
=============

Version: 1.1.3
- lhcsmapi >= 1.7.0, and take into account the new circuit types from lhc-sm-api
- black formatting
- sonar fixes

Version: 1.1.2
- pandas < 2.0.0: [SIGMON-483](https://its.cern.ch/jira/browse/SIGMON-483)


Version: 1.1.1
- preserve the order of the discharges passed to the query function : [SIGMON-467](https://its.cern.ch/jira/browse/SIGMON-467)
- clean context before querying (memory overflow bugfix) : [SIGMON-467](https://its.cern.ch/jira/browse/SIGMON-467)


Version: 1.1.0
- summary table sorted by timestamp and circuit_name: [SIGMON-356](https://its.cern.ch/jira/browse/SIGMON-356)
- tests written (for the ccc case): [SIGMON-354](https://its.cern.ch/jira/browse/SIGMON-354)
- `openjdk:8` used for sonar: [SIGMON-411](https://its.cern.ch/jira/browse/SIGMON-411)
- replace the old QHDA notebooks (support similar outputs): [SIGMON-397](https://its.cern.ch/jira/browse/SIGMON-397)
- code quality improvements (and repository housekeeping): [SIGMON-415](https://its.cern.ch/jira/browse/SIGMON-415)
- added acceptance criteria to the README: [SIGMON-362](https://its.cern.ch/jira/browse/SIGMON-362)
- dummy tests added for the `output.py`: [SIGMON-362](https://its.cern.ch/jira/browse/SIGMON-362)
- updated thresholds for the final voltage: [SIGMON-427](https://its.cern.ch/jira/browse/SIGMON-427)


Version: 1.0.1
- formatting of the floating point numbers in the comparison tables changed [SIGMON-371](https://its.cern.ch/jira/browse/SIGMON-371)