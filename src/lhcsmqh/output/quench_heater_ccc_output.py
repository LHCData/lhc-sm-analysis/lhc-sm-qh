"""Module containing functions specific to the presentation of the results in the CCC."""

from __future__ import annotations

from lhcsmapi.api.analysis.output import output_types

from lhcsmqh.analyses import QHCCCAnalysis, commons

from . import _common as output_common


def get_summary(analysis: QHCCCAnalysis) -> list[output_types.Output]:
    """Returns the summary of the analysis as an HTML table with each row being an analyzed event.

    Example:

    | source | datetime                | circuit_type | circuit_name | analysis_result |
    |--------|-------------------------|--------------|--------------|-----------------|
    | A28L3  | 2021-10-18 09:42:29.913 | RB           | RB.A23       | Passed          |
    | RQ5.L8 | 2021-10-11 12:18:19.979 | IPQ          | RQ5.L8       | Passed          |

    Args:
        analysis: QHCCCAnalysis object

    Returns:
        A list of Output instances with only one HTMLOutput."""

    events: list[commons.VoltageEvent | commons.VoltageCurrentEvent] = []
    events.extend(analysis.voltage_events)
    events.extend(analysis.voltage_current_events)

    results: list[commons.VoltageResult | commons.VoltageCurrentResult] = []
    results.extend(analysis.voltage_results)
    results.extend(analysis.voltage_current_results)
    return output_common.get_summary_output(events, results)


def get_output(analysis: QHCCCAnalysis) -> list[output_types.Output]:
    """Returns the output of the analysis as a list of Output instances.

    Args:
        analysis: QHCCCAnalysis object

    Returns:
        A list of Output instances."""

    output_list: list[output_types.Output] = []

    if analysis.initial_charge_check and not analysis.low_charge_events.empty:
        output_list.append(output_types.TextOutput("Low level charge events:"))
        output_list.append(output_types.HTMLOutput(analysis.low_charge_events.to_html()))

    if len(analysis.voltage_events) + len(analysis.voltage_current_events) == 0:
        output_list.append(
            output_types.HTMLOutput(
                '<h2><span style="background-color: #3DCF1A">&nbsp&nbsp&nbsp'
                "There were no QH discharges on selected time range!&nbsp&nbsp&nbsp</span></h2>"
            )
        )
        return output_list

    for voltage_event, voltage_result in zip(analysis.voltage_events, analysis.voltage_results, strict=True):
        output_list.extend(output_common.get_voltage_output(voltage_event, voltage_result))

    for voltage_current_event, voltage_current_result in zip(
        analysis.voltage_current_events, analysis.voltage_current_results, strict=True
    ):
        output_list.extend(output_common.get_voltage_current_output(voltage_current_event, voltage_current_result))

    signature = (
        '<span style="background-color: #3DCF1A">&nbsp&nbsp&nbsp Passed'
        if analysis.get_analysis_output()
        else '<span style="background-color: red">&nbsp&nbsp&nbsp&nbsp Failed'
    )

    output_list.append(output_types.HTMLOutput(f"Overall signature = <h2>{signature} &nbsp&nbsp&nbsp&nbsp</span></h2>"))

    return output_list
