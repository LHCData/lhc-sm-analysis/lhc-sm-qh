"""Module containing functions presenting the results of the QHDA for IPQ/IPD/IT circuits."""

from __future__ import annotations

from collections.abc import Sequence

from lhcsmapi.api.analysis.output import output_types

from lhcsmqh.analyses import QuenchHeaterVoltageAnalysis, commons

from . import _common as output_common


def get_summary(analysis: QuenchHeaterVoltageAnalysis) -> list[output_types.Output]:
    """Returns the summary of the analysis as an HTML table with each row being an analyzed event.

    Example:

    | source | timestamp           | datetime                | analysis_result | link_to_analysis |
    |--------| ------------------- |-------------------------|-----------------|------------------|
    | A28L3  | 1621447239984999936 | 2021-10-18 09:42:29.913 | Passed          | <link>           |
    | A28L3  | 1621447239984999936 | 2021-10-11 12:18:19.979 | Passed          | <link>           |

    Args:
        analysis: QuenchHeaterVoltageCurrentAnalysis object

    Returns:
        A list of Output instances with only one HTMLOutput."""

    return output_common.get_summary_with_links_output(analysis.voltage_events, analysis.voltage_results)


def get_output(analysis: QuenchHeaterVoltageAnalysis) -> list[output_types.Output]:
    """Returns the output of the analysis as a list of Output instances.

    Args:
        analysis: QuenchHeaterVoltageCurrentAnalysis object

    Returns:
        A list of Output instances."""

    def add_outputs_with_links(
        outputs: list[output_types.Output],
        events: Sequence[commons.VoltageEvent],
        results: Sequence[commons.VoltageResult],
    ) -> None:
        for event, result in zip(events, results, strict=True):
            outputs.append(
                output_types.HTMLOutput(
                    f"<a id={event.source}{event.signals.timestamp!s}>{event.source}</a> "
                    f"<a href=#{output_common.SUMMARY_TABLE_ID}>Back to Table</a>"
                )
            )
            outputs.extend(output_common.get_voltage_output(event, result))

    return output_common.get_output_wrapper(
        analysis.get_analysis_output, analysis.voltage_events, analysis.voltage_results, add_outputs_with_links
    )
