"""Module containing common logic used to present the results of the QH analysis."""

from __future__ import annotations

from collections.abc import Callable, Sequence
from typing import Any

import numpy as np
import pandas as pd
from lhcsmapi import Time
from lhcsmapi.api.analysis.output import output_types
from matplotlib import pyplot as plt

from lhcsmqh import analyses

FONT_SIZE = 15
SUMMARY_TABLE_ID = "Table"


def get_summary_output(
    events: Sequence[analyses.VoltageEvent | analyses.VoltageCurrentEvent],
    results: Sequence[analyses.VoltageResult | analyses.VoltageCurrentResult],
) -> list[output_types.Output]:

    df_result = pd.DataFrame(
        {
            "source": event.source,
            "datetime": Time.Time.to_string_short(event.signals.timestamp),
            "circuit_type": event.circuit_type,
            "circuit_name": event.circuit_name,
            "analysis_result": "PASSED" if result else "FAILED",
        }
        for event, result in zip(events, results, strict=True)
    )
    return [output_types.HTMLOutput(df_result.to_html())]


def get_summary_with_links_output(
    events: Sequence[analyses.VoltageEvent | analyses.VoltageCurrentEvent],
    results: Sequence[analyses.VoltageResult | analyses.VoltageCurrentResult],
) -> list[output_types.Output]:
    def get_row(
        event: analyses.VoltageEvent | analyses.VoltageCurrentEvent,
        result: analyses.VoltageResult | analyses.VoltageCurrentResult,
    ) -> list[Any]:
        return [
            event.source,
            int(event.signals.timestamp),
            Time.Time.to_string_short(event.signals.timestamp),
            "PASSED" if result else "FAILED",
            '<a href="#' + event.source + str(event.signals.timestamp) + '">' + event.source + "</a>",
        ]

    rows = [get_row(event, result) for event, result in zip(events, results, strict=True)]
    columns = ["source", "timestamp", "datetime", "analysis_result", "link_to_analysis"]

    df_result = pd.DataFrame(rows, columns=columns)
    return [output_types.HTMLOutput("<a id=Table>Table</a>"), output_types.HTMLOutput(df_result.to_html(escape=False))]


def get_voltage_output(event: analyses.VoltageEvent, result: analyses.VoltageResult) -> list[output_types.Output]:
    output_list: list[output_types.Output] = []

    datetime_string = Time.Time.to_string_short(event.signals.timestamp)
    reference_datetime_string = Time.Time.to_string_short(event.reference_signals.timestamp)

    def plot_figure(title: str, log_scale: bool = False) -> plt.Figure:
        fig, ax = plt.subplots(1, 1, figsize=(15, 7))

        ax.set_title(title, fontsize=FONT_SIZE * 4 / 3)
        ax.set_ylabel("U_HDS, [V]", fontsize=FONT_SIZE)
        ax.set_xlabel("time, [s]", fontsize=FONT_SIZE)
        ax.set_xlim((-0.5, 1.5))

        if log_scale:
            ax.set_yscale("log")

        for u_hds, u_hds_ref in zip(event.signals.u_hds, event.reference_signals.u_hds, strict=True):
            u_hds.plot(ax=ax)
            u_hds_ref.plot(ax=ax, style="--")

        ax.grid(True)

        return fig

    output_list.append(
        output_types.FigureOutput(
            plot_figure(f"Magnet: {event.source}, Time Stamp: {datetime_string}, U_HDS(t)", False)
        )
    )
    output_list.append(
        output_types.FigureOutput(
            plot_figure(f"Magnet: {event.source}, Time Stamp: {datetime_string}, U_HDS(t), Log Scale", True)
        )
    )

    row_color_dct = {False: "background-color: red", True: "", np.nan: ""}

    first_last_u_comp = result.first_last_u_comp
    tau_u_comp = result.tau_u_comp
    html = (
        first_last_u_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the initial and final voltage")
        .apply(lambda s: first_last_u_comp["result"].map(row_color_dct))
        .format(precision=1)
        .to_html()
        + tau_u_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the discharge characteristic time to the reference")
        .apply(lambda s: tau_u_comp["result"].map(row_color_dct))
        .format(precision=3)
        .to_html()
    )

    output_list.append(output_types.HTMLOutput(html))

    text = (
        f"The QH Discharges are labeled: {bool(result)}."
        f"<br>Reference QH discharge timestamp is: {reference_datetime_string}"
    )

    output_list.append(output_types.HTMLOutput(text))

    return output_list


def get_voltage_current_output(
    event: analyses.VoltageCurrentEvent, result: analyses.VoltageCurrentResult
) -> list[output_types.Output]:
    output_list: list[output_types.Output] = []

    datetime_string = Time.Time.to_string_short(event.signals.timestamp)
    reference_datetime_string = Time.Time.to_string_short(event.reference_signals.timestamp)

    title = f"Magnet: {event.source}, Time Stamp: {datetime_string}, U_HDS(t), I_HDS(t), R_HDS(t)"

    fig, ax = plt.subplots(1, 3, figsize=(20, 7))

    for u_hds, u_hds_ref in zip(event.signals.u_hds_sync_dfs, event.reference_signals.u_hds_sync_dfs, strict=True):
        u_hds.plot(ax=ax[0])
        u_hds_ref.plot(ax=ax[0], style="--")

    ax[0].grid(True)
    ax[0].tick_params(labelsize=FONT_SIZE)
    ax[0].set_xlabel("time, [s]", fontsize=FONT_SIZE)
    ax[0].set_ylabel("U_HDS, [V]", fontsize=FONT_SIZE)

    for i_hds, i_hds_ref in zip(event.signals.i_hds_sync_dfs, event.reference_signals.i_hds_sync_dfs, strict=True):
        i_hds.plot(ax=ax[1])
        i_hds_ref.plot(ax=ax[1], style="--")

    ax[1].set_title(title, fontsize=FONT_SIZE * 4 / 3)
    ax[1].grid(True)
    ax[1].tick_params(labelsize=FONT_SIZE)
    ax[1].set_xlabel("time, [s]", fontsize=FONT_SIZE)
    ax[1].set_ylabel("I_HDS, [A]", fontsize=FONT_SIZE)

    for r_hds, r_hds_ref in zip(event.signals.r_hds, event.reference_signals.r_hds, strict=True):
        r_hds.plot(ax=ax[2])
        r_hds_ref.plot(ax=ax[2], style="--")

    ax[2].grid(True)
    ax[2].tick_params(labelsize=FONT_SIZE)
    ax[2].set_xlabel("time, [s]", fontsize=FONT_SIZE)
    ax[2].set_ylabel("R_HDS (Calculated), [Ohm]", fontsize=FONT_SIZE)

    output_list.append(output_types.FigureOutput(fig))

    row_color_dct = {False: "background-color: red", True: "", np.nan: ""}
    first_last_u_comp = result.first_last_u_comp
    first_r_comp = result.first_r_comp
    tau_u_comp = result.tau_u_comp
    capacitance_comp = result.capacitance_comp
    html = (
        first_last_u_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the initial and final voltage")
        .apply(lambda s: first_last_u_comp["result"].map(row_color_dct))
        .format(precision=1)
        .to_html()
        + first_r_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the initial resistance to the reference")
        .apply(lambda s: first_r_comp["result"].map(row_color_dct))
        .format(precision=2)
        .to_html()
        + tau_u_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the discharge characteristic time to the reference")
        .apply(lambda s: tau_u_comp["result"].map(row_color_dct))
        .format(precision=3)
        .to_html()
        + capacitance_comp.style.set_table_attributes("style='display:inline;vertical-align:top'")
        .set_caption("Comparison of the estimated capacitance to the reference")
        .apply(lambda s: capacitance_comp["result"].map(row_color_dct))
        .format(precision=3)
        .to_html()
    )

    output_list.append(output_types.HTMLOutput(html))

    text = (
        f"The QH Discharges are labeled: {bool(result)}."
        f"<br>Reference QH discharge timestamp is: {reference_datetime_string}"
    )

    output_list.append(output_types.HTMLOutput(text))

    return output_list


def get_output_wrapper(
    get_analysis_result: Callable[[], bool],
    events: Sequence[analyses.VoltageEvent | analyses.VoltageCurrentEvent],
    results: Sequence[analyses.VoltageResult | analyses.VoltageCurrentResult],
    add_outputs: Callable[[list[output_types.Output], Sequence[Any], Sequence[Any]], None],
) -> list[output_types.Output]:
    outputs: list[output_types.Output] = []

    if len(events) == 0:
        outputs.append(
            output_types.HTMLOutput(
                '<h2><span style="background-color: #3DCF1A">&nbsp&nbsp&nbsp'
                "There were no QH discharges on selected time range!&nbsp&nbsp&nbsp</span></h2>"
            )
        )
        return outputs

    add_outputs(outputs, events, results)

    outputs.append(output_types.HTMLOutput(f"Overall signature = <h2>{get_analysis_result()}</h2>"))

    return outputs
