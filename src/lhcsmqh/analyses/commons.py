"""Module containing common logic used to analyze QH events."""

from __future__ import annotations

import datetime
from collections.abc import Sequence
from dataclasses import dataclass, fields
from typing import Any

import numpy as np
import pandas as pd
from lhcsmapi import reference
from lhcsmapi.analysis import comparison  # type: ignore
from lhcsmapi.api import processing, query, resolver
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import CircuitType, GenericCircuitType
from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion  # type: ignore
from lhcsmapi.signal_analysis import features as signal_analysis  # type: ignore
from lhcsmapi.signal_analysis import functions as signal_analysis_functions
from lhcsmapi.Time import Time
from pyspark.sql import SparkSession


def _check_list_dfs(first: list[pd.DataFrame], second: list[pd.DataFrame]) -> bool:
    return len(first) == len(second) and all(f.equals(s) for f, s in zip(first, second, strict=True))


@dataclass(frozen=True)
class Event:
    """Base class for a discharge event."""

    source: str
    circuit_type: str
    circuit_name: str

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, Event):
            return False

        return (
            self.source == other.source
            and self.circuit_type == other.circuit_type
            and self.circuit_name == other.circuit_name
        )


@dataclass(frozen=True)
class VoltageSignals:
    timestamp: int
    u_hds: list[pd.DataFrame]

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageSignals):
            return False

        return self.timestamp == other.timestamp and _check_list_dfs(self.u_hds, other.u_hds)


@dataclass(frozen=True)
class VoltageEvent(Event):
    signals: VoltageSignals
    reference_signals: VoltageSignals

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageEvent):
            return False

        return (
            super().__eq__(other)
            and self.signals == other.signals
            and self.reference_signals == other.reference_signals
        )


@dataclass(frozen=True)
class VoltageCurrentSignals:
    timestamp: int
    u_hds_dfs: list[pd.DataFrame]
    u_hds_sync_dfs: list[pd.DataFrame]
    u_hds_decay_dfs: list[pd.DataFrame]
    u_hds_decay_sync_dfs: list[pd.DataFrame]
    i_hds_dfs: list[pd.DataFrame]
    i_hds_sync_dfs: list[pd.DataFrame]
    i_hds_decay_dfs: list[pd.DataFrame]
    i_hds_decay_sync_dfs: list[pd.DataFrame]
    r_hds: list[pd.DataFrame]

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageCurrentSignals):
            return False

        return all(
            (
                getattr(self, field.name) == getattr(other, field.name)
                if isinstance(getattr(self, field.name), int)
                else _check_list_dfs(getattr(self, field.name), getattr(other, field.name))
            )
            for field in fields(self)
        )


@dataclass(frozen=True)
class VoltageCurrentEvent(Event):

    signals: VoltageCurrentSignals
    reference_signals: VoltageCurrentSignals

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageCurrentEvent):
            return False

        return (
            super().__eq__(other)
            and self.signals == other.signals
            and self.reference_signals == other.reference_signals
        )


def repr_dataframe(df: pd.DataFrame) -> str:
    return f"pd.DataFrame({df.to_dict()})"


@dataclass(frozen=True)
class VoltageResult:
    """QH Analysis result for IPD/IPQ/IT circuits.

    Args:
        source: source of the event
        timestamp: timestamp of the event
        first_last_u_comp: `pd.DataFrame` with `first` and `last20mean` features calculated for every voltage signal
            from the event, with comparison with reference values. The `result` column is a boolean value indicating
            whether the values are inside ranges specified by the reference.
        tau_u_comp: `pd.DataFrame` with `tau_charge` feature calculated for every voltage signal
            from the event, with comparison with reference values. The `result` column is a boolean value indicating
            whether the values are inside ranges specified by the reference.
    """

    source: str
    timestamp: int
    first_last_u_comp: pd.DataFrame
    tau_u_comp: pd.DataFrame

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageResult):
            return False

        return (
            self.source == other.source
            and self.timestamp == other.timestamp
            and self.first_last_u_comp.equals(other.first_last_u_comp)
            and self.tau_u_comp.equals(other.tau_u_comp)
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(source={self.source!r}, timestamp={self.timestamp}, "
            f"first_last_u_comp={repr_dataframe(self.first_last_u_comp)}, "
            f"tau_u_comp={repr_dataframe(self.tau_u_comp)})"
        )

    def __bool__(self) -> bool:
        return bool(self.first_last_u_comp["result"].all() and self.tau_u_comp["result"].all())


@dataclass(frozen=True)
class VoltageCurrentResult(VoltageResult):
    """QH Analysis result for RB/RQ circuits.

    Args:
        source: source of the event
        timestamp: timestamp of the event
        first_last_u_comp: `pd.DataFrame` with `first` and `last20mean` features calculated for every voltage signal
            from the event, with comparison with reference values. The `result` column is a boolean value indicating
            whether the values are inside ranges specified by the reference.
        tau_u_comp: `pd.DataFrame` with `tau_charge` feature calculated for every voltage signal
            from the event, with comparison with reference values. The `result` column is a boolean value indicating
            whether the values are inside ranges specified by the reference.
        first_r_comp: `pd.DataFrame` with `first20mean` feature calculated for every resistance signal
            from the event, with comparison with reference values. The `result` column is a boolean value indicating
            whether the values are inside ranges specified by the reference.
        capacitance_comp: `pd.DataFrame` with calculated capacitance values, with comparison with reference values.
            The `result column is a boolean value indicating whether the values are inside ranges specified
            by the reference.
    """

    first_r_comp: pd.DataFrame
    capacitance_comp: pd.DataFrame

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, VoltageCurrentResult):
            return False

        return (
            super().__eq__(other)
            and self.first_r_comp.equals(other.first_r_comp)
            and self.capacitance_comp.equals(other.capacitance_comp)
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(source={self.source!r}, timestamp={self.timestamp}, "
            f"first_last_u_comp={repr_dataframe(self.first_last_u_comp)}, "
            f"tau_u_comp={repr_dataframe(self.tau_u_comp)}, "
            f"first_r_comp={repr_dataframe(self.first_r_comp)}, "
            f"capacitance_comp={repr_dataframe(self.capacitance_comp)})"
        )

    def __bool__(self) -> bool:
        return bool(super().__bool__() and self.first_r_comp["result"].all() and self.capacitance_comp["result"].all())


def filter_median(hds_dfs: list[pd.DataFrame], window: int = 3) -> list[pd.DataFrame]:
    return [hds_df.rolling(window=window, min_periods=1).median() for hds_df in hds_dfs]


def get_start_index_with_current_mean_std(i_hds_df: pd.DataFrame, index_increment: int, mean_start_value: float) -> int:
    def has_window_mean_std(x: np.ndarray, mean: float = 50, std: float = 0.1) -> bool:
        return (x.mean() >= mean) and (x.std() <= std)

    with_mean_std_df = (
        i_hds_df.rolling(index_increment)
        .apply(has_window_mean_std, args=(mean_start_value,), raw=True)
        .shift(-index_increment + 1)
    )

    with_mean_std_df.dropna(inplace=True)
    mask = with_mean_std_df[with_mean_std_df[with_mean_std_df.columns[0]] == 1.0]
    if len(mask) == 0:
        return 0

    return with_mean_std_df.index.get_loc(mask.index[0])  # type: ignore


def get_decay_start_index(
    i_hds_dfs: list[pd.DataFrame], mean_start_value: float = 50, index_increment: int = 20
) -> int:
    decay_start_index = 0
    for i_hds_df in i_hds_dfs:
        i_decay_start_index = get_start_index_with_current_mean_std(i_hds_df, index_increment, mean_start_value)
        decay_start_index = max(decay_start_index, i_decay_start_index)

    return decay_start_index


def extract_decay(hds_dfs: list[pd.DataFrame], index_decay_start: int) -> list[pd.DataFrame]:
    if index_decay_start == 0:
        return hds_dfs

    return [hds_df.iloc[(index_decay_start + 1) : len(hds_df)] for hds_df in hds_dfs]


def preprocess_voltage_current(
    u_hds_dfs: list[pd.DataFrame], i_hds_dfs: list[pd.DataFrame], current_offset: float, mean_start_value: float
) -> tuple[list[pd.DataFrame], list[pd.DataFrame]]:
    # Subtract current offset
    i_hds_no_offset_dfs = [i_hds_df - current_offset for i_hds_df in i_hds_dfs if not i_hds_df.empty]

    # Find start of decay
    index_decay_start = get_decay_start_index(i_hds_dfs, mean_start_value)

    # Extract decay only
    u_hds_decay_dfs = extract_decay(u_hds_dfs, index_decay_start)
    i_hds_no_offset_decay_dfs = extract_decay(i_hds_no_offset_dfs, index_decay_start)

    # Median filter
    return filter_median(u_hds_decay_dfs), filter_median(i_hds_no_offset_decay_dfs)


def calculate_resistance(u_hds_dfs: list[pd.DataFrame], i_hds_dfs: list[pd.DataFrame]) -> list[pd.DataFrame]:
    r_hds_dfs = []
    for u_hds_df, i_hds_df in zip(u_hds_dfs, i_hds_dfs, strict=True):
        # suppresses "divide by zero encountered in divide" warnings - we know it's +-inf
        with np.errstate(divide="ignore"):
            r_value = u_hds_df.values / i_hds_df.values
        column = u_hds_df.columns[0].replace("U", "R")
        time = u_hds_df.index
        r_hds_dfs.append(pd.DataFrame(data=r_value, index=time, columns=[column]))

    return r_hds_dfs


def calculate_capacitance(tau_df: pd.DataFrame, first_r_df: pd.DataFrame) -> pd.DataFrame:
    """Calculates capacitance values for Quench Heater circuits using tau and resistance measurements.

    Uses the RC circuit relationship: C = tau/R, where:
    - tau is the time constant from voltage decay curves
    - R is the initial resistance measurement

    Args:
        tau_df: DataFrame containing time constants (tau) calculated from voltage decay analysis.
               Expected columns format: "<circuit>:U_HDS_<number>:tau_charge"
        first_r_df: DataFrame containing initial resistance measurements.
                   Expected columns format: "<circuit>:R_HDS_<number>:first20mean"

    Returns:
        DataFrame with calculated capacitance values.
        Column format: "<circuit>_C_HDS_<number>:capacitance"
    """

    def get_transposed_with_feature_name(df: pd.DataFrame) -> pd.DataFrame:
        """Helper function to transpose DataFrame and rename indices for capacitance calculation.

        Args:
            df: Input DataFrame with voltage or resistance measurements

        Returns:
            Transposed DataFrame with renamed indices following capacitance naming convention
        """

        def get_feature_name(old_name: str) -> str:
            """Converts signal names to capacitance feature names.

            Args:
                old_name: Original signal name from voltage or resistance measurements

            Returns: Capacitance feature name

            Examples:
                >>> get_feature_name("MQXFA.A2R1:U_HDS_1:tau_charge")
                "MQXFA.A2R1_C_HDS_1:capacitance"
            """
            circuit_name = old_name.split(":")[0]
            heater_id = old_name.split(":")[-2].split("_")[-1]  # e.g., "1" from "U_HDS_1" or "R_HDS_1"

            return f"{circuit_name}_C_HDS_{heater_id}:capacitance"

        transposed = df.T
        transposed.index = transposed.index.map(get_feature_name)
        return transposed

    tau_df_transposed = get_transposed_with_feature_name(tau_df.filter(regex="(.*):U_(.*)"))
    first_r_df_transposed = get_transposed_with_feature_name(first_r_df)

    # Calculate capacitance values using C = tau/R
    tau_df_transposed.iloc[:, 0] = tau_df_transposed.iloc[:, 0] / first_r_df_transposed.iloc[:, 0]
    return tau_df_transposed.T


def query_voltage_current_signals(
    circuit_type: str,
    circuit_name: str,
    source: str,
    timestamp: int,
    current_offset: float,
    mean_start_value: float = 50,
) -> VoltageCurrentSignals:
    u_hds_dfs = query_single_qh_event_pm(circuit_type, circuit_name, source, timestamp, "U_HDS")
    i_hds_dfs = query_single_qh_event_pm(circuit_type, circuit_name, source, timestamp, "I_HDS")

    u_hds_decay_dfs, i_hds_decay_dfs = preprocess_voltage_current(
        u_hds_dfs, i_hds_dfs, current_offset, mean_start_value
    )

    # Synchronize time of the raw signal from PM to 0
    # # For plotting
    i_index_to_sync = i_hds_decay_dfs[0].index[0]
    u_index_to_sync = u_hds_decay_dfs[0].index[0]

    u_hds_sync_dfs: list[pd.DataFrame] = [
        SignalIndexConversion.synchronize_df(hds_df, u_index_to_sync) for hds_df in u_hds_dfs  # type: ignore
    ]
    i_hds_sync_dfs: list[pd.DataFrame] = [
        SignalIndexConversion.synchronize_df(hds_df, i_index_to_sync) for hds_df in i_hds_dfs  # type: ignore
    ]

    # # For feature engineering
    u_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(u_hds_decay_dfs)
    i_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(i_hds_decay_dfs)

    # Calculate resistance
    r_hds_dfs = calculate_resistance(u_hds_decay_sync_dfs, i_hds_decay_sync_dfs)
    return VoltageCurrentSignals(
        timestamp=timestamp,
        u_hds_dfs=u_hds_dfs,
        u_hds_sync_dfs=u_hds_sync_dfs,
        u_hds_decay_dfs=u_hds_decay_dfs,
        u_hds_decay_sync_dfs=u_hds_decay_sync_dfs,
        i_hds_dfs=i_hds_dfs,
        i_hds_sync_dfs=i_hds_sync_dfs,
        i_hds_decay_dfs=i_hds_decay_dfs,
        i_hds_decay_sync_dfs=i_hds_decay_sync_dfs,
        r_hds=r_hds_dfs,
    )


def query_voltage_current_event(
    circuit_type: str, circuit_name: str, source: str, timestamp: int, current_offset: float, mean_start_value: float
) -> VoltageCurrentEvent:
    signals = query_voltage_current_signals(
        circuit_type, circuit_name, source, timestamp, current_offset, mean_start_value
    )
    reference_signals = query_voltage_current_signals(
        circuit_type,
        circuit_name,
        source,
        reference.get_quench_heater_reference_discharge(
            signal_metadata.get_circuit_type_for_circuit_name(circuit_name), source, timestamp
        ),
        current_offset,
        mean_start_value,
    )
    return VoltageCurrentEvent(
        source=source,
        circuit_type=circuit_type,
        circuit_name=circuit_name,
        signals=signals,
        reference_signals=reference_signals,
    )


def first(signal: pd.Series) -> float:
    return signal.iloc[0]


def first20mean(signal: pd.Series) -> float:
    return signal.iloc[:20].mean()


def last20mean(signal: pd.Series) -> float:
    return signal.iloc[-20:].mean()


def analyze_voltage_current_event(
    voltage_current_event: VoltageCurrentEvent, discharge_level: int
) -> VoltageCurrentResult:
    # Calculate features
    first_last_u_df = signal_analysis.calculate_features(
        voltage_current_event.signals.u_hds_decay_sync_dfs, [first, last20mean], voltage_current_event.signals.timestamp
    )
    first_last_u_comp_df = comparison.compare_features_to_reference(
        first_last_u_df,
        voltage_current_event.circuit_type,
        "QH",
        wildcard={"CELL": voltage_current_event.source},
        nominal_voltage=discharge_level,
    )

    first_r_df = signal_analysis.calculate_features(
        voltage_current_event.signals.r_hds, first20mean, voltage_current_event.signals.timestamp
    )
    first_r_ref_df = signal_analysis.calculate_features(
        voltage_current_event.reference_signals.r_hds, first20mean, voltage_current_event.reference_signals.timestamp
    )
    first_r_comp_df = comparison.compare_difference_of_features_to_reference(
        first_r_df,
        first_r_ref_df,
        voltage_current_event.circuit_type,
        "QH",
        wildcard={"CELL": voltage_current_event.source},
        nominal_voltage=discharge_level,
        precision=2,
    )

    tau_df = signal_analysis.calculate_features(
        voltage_current_event.signals.u_hds_decay_sync_dfs + voltage_current_event.signals.i_hds_decay_sync_dfs,
        signal_analysis_functions.tau_charge,
        voltage_current_event.signals.timestamp,
    )
    tau_ref_df = signal_analysis.calculate_features(
        voltage_current_event.reference_signals.u_hds_decay_sync_dfs
        + voltage_current_event.reference_signals.i_hds_decay_sync_dfs,
        signal_analysis_functions.tau_charge,
        voltage_current_event.reference_signals.timestamp,
    )
    tau_comp_df = comparison.compare_difference_of_features_to_reference(
        tau_df,
        tau_ref_df,
        voltage_current_event.circuit_type,
        "QH",
        wildcard={"CELL": voltage_current_event.source},
        nominal_voltage=discharge_level,
        precision=3,
    )

    capacitance_df = calculate_capacitance(tau_df, first_r_df)
    capacitance_ref_df = calculate_capacitance(tau_ref_df, first_r_ref_df)
    capacitance_comp_df = comparison.compare_difference_of_features_to_reference(
        capacitance_df, capacitance_ref_df, voltage_current_event.circuit_type, "QH", precision=3
    )

    return VoltageCurrentResult(
        source=voltage_current_event.source,
        timestamp=voltage_current_event.signals.timestamp,
        first_last_u_comp=first_last_u_comp_df,
        first_r_comp=first_r_comp_df,
        tau_u_comp=tau_comp_df,
        capacitance_comp=capacitance_comp_df,
    )


def find_source_timestamp_qh(circuit_type: str, circuit_name: str, start_time: int, stop_time: int) -> pd.DataFrame:
    pm_params = resolver.get_params_for_pm_events(
        circuit_type=circuit_type,
        circuit_name=circuit_name,
        system="QH",
        timestamp=start_time,
        duration=stop_time - start_time,
        wildcard={"CELL": "*"},
    )

    events = query.query_pm_events_with_resolved_params(pm_params)

    if not events.empty:
        events = (
            processing.EventProcessing(events)
            .filter_source(circuit_type, circuit_name, "QH")
            .sort_values(by=["timestamp", "source"])
            .drop_duplicates(column=["source", "timestamp"])  # type: ignore
            .get_dataframe()
        )
        events["circuit_type"] = circuit_type
        events["circuit_name"] = circuit_name

    return events


def query_single_qh_event_pm(
    circuit_type: str, circuit_name: str, source: str, timestamp: int, signals: str
) -> list[pd.DataFrame]:
    pm_params = resolver.get_params_for_pm_signals(
        circuit_type, circuit_name, "QH", timestamp, signals=signals, wildcard={"CELL": source}
    )

    query_result = query.query_pm_signals_with_resolved_params(pm_params)

    return (
        processing.SignalProcessing(query_result)
        .synchronize_time(timestamp)
        .convert_index_to_sec()
        .drop_first_n_points(5)
        .drop_last_n_points(5)
        .get_dataframes()
    )


def query_single_qh_event_nxcals(
    spark: SparkSession, circuit_type: str, circuit_name: str, source: str, timestamp: int, duration: int, signals: str
) -> list[pd.DataFrame]:
    nxcals_params = resolver.get_params_for_nxcals(
        circuit_type, circuit_name, "QH", timestamp, duration, signals=signals, wildcard={"MAGNET": source}
    )

    return query.query_nxcals_by_variables(spark, nxcals_params)


def _extract_voltage_decay(u_hds_dfs: list[pd.DataFrame]) -> list[pd.DataFrame]:
    u_hds_decay_dfs = []
    for u_hds_df in u_hds_dfs:
        max_value = u_hds_df.max().values[0]
        min_value = u_hds_df.min().values[0]
        min_index = u_hds_df.idxmin().values[0]
        if max_value > min_value:
            u_hds_decay_dfs.append(u_hds_df[(u_hds_df.index >= 0) & (u_hds_df.index < min_index)])
        else:
            u_hds_decay_dfs.append(u_hds_df[(u_hds_df.index >= 0)])

    return u_hds_decay_dfs


def fill_value_at_location_0_with_latest_preceding(df: pd.DataFrame) -> pd.DataFrame:
    """
    Fill the value at location 0 with the latest preceding value if the dataframe does not have a value at location 0
    See https://its.cern.ch/jira/browse/SIGMON-629
    """
    if 0 not in df.index and not df.loc[:0].empty:
        df.loc[0] = df.loc[:0].iloc[-1]
    return df.sort_index()


def query_voltage_signals(circuit_type: str, circuit_name: str, source: str, timestamp: int) -> VoltageSignals:
    return VoltageSignals(timestamp, query_single_qh_event_pm(circuit_type, circuit_name, source, timestamp, "U_HDS"))


def query_voltage_event(circuit_type: str, circuit_name: str, source: str, timestamp: int) -> VoltageEvent | None:
    voltage_signals = query_voltage_signals(circuit_type, circuit_name, source, timestamp)
    if all(df.empty for df in voltage_signals.u_hds):
        # Because QH pm data dumps have the same class name as other systems (e.g QDS, LEADS),
        # it is impossible to distinguish between them when querying pm data headers.
        # The only way to know is to check if u_hds is empty or not. If it is empty, it is not a QH event.
        return None

    return VoltageEvent(
        source,
        circuit_type,
        circuit_name,
        voltage_signals,
        query_voltage_signals(
            circuit_type,
            circuit_name,
            source,
            reference.get_quench_heater_reference_discharge(
                signal_metadata.get_circuit_type_for_circuit_name(circuit_name), source, timestamp
            ),
        ),
    )


def analyze_voltage_event(voltage_event: VoltageEvent, discharge_level: float) -> VoltageResult:
    u_dfs = voltage_event.signals.u_hds
    u_dfs_ref = voltage_event.reference_signals.u_hds

    meta_circuit_type = signal_metadata.get_circuit_type_for_circuit_name(voltage_event.circuit_name)

    # For the IPQ, IPD and IT circuits the sampling rate changes at PM timestamp (0 here).
    # Sometimes the change is too slow and hence we want to treat the previous point as the 'first' one of the decay
    # see https://its.cern.ch/jira/browse/SIGMON-629
    if (
        signal_metadata.is_ipd(voltage_event.circuit_name)
        or signal_metadata.is_ipq(voltage_event.circuit_name)
        or signal_metadata.is_inner_triplet(voltage_event.circuit_name)
    ):
        u_dfs = [fill_value_at_location_0_with_latest_preceding(u_df) for u_df in u_dfs]
        u_dfs_ref = [fill_value_at_location_0_with_latest_preceding(u_df) for u_df in u_dfs_ref]

    u_hds_decay_dfs = _extract_voltage_decay(u_dfs)
    u_hds_decay_dfs_ref = _extract_voltage_decay(u_dfs_ref)

    first_last_u_df = signal_analysis.calculate_features(
        u_hds_decay_dfs, [first, last20mean], voltage_event.signals.timestamp
    )
    first_last_u_comp_df = comparison.compare_features_to_reference(
        first_last_u_df,
        meta_circuit_type,
        "QH",
        wildcard={"CIRCUIT": voltage_event.circuit_name},
        nominal_voltage=discharge_level,
    )

    #  tau charge is calculated always from 0 (which is the PM timestamp)
    u_hds_decay_dfs = [df.loc[0:, :] for df in u_hds_decay_dfs]
    u_hds_decay_dfs_ref = [df.loc[0:, :] for df in u_hds_decay_dfs_ref]
    tau_u_df = signal_analysis.calculate_features(
        u_hds_decay_dfs, signal_analysis_functions.tau_charge, voltage_event.signals.timestamp
    )
    tau_u_df_ref = signal_analysis.calculate_features(
        u_hds_decay_dfs_ref, signal_analysis_functions.tau_charge, voltage_event.reference_signals.timestamp
    )
    tau_u_comp_df = comparison.compare_difference_of_features_to_reference(
        tau_u_df,
        tau_u_df_ref,
        meta_circuit_type,
        "QH",
        wildcard={"CIRCUIT": voltage_event.circuit_name},
        nominal_voltage=discharge_level,
        precision=3,
    )

    return VoltageResult(voltage_event.source, voltage_event.signals.timestamp, first_last_u_comp_df, tau_u_comp_df)


DETAILED_CIRCUIT_TYPES_MAP = {
    GenericCircuitType.RB: [CircuitType.RB],
    GenericCircuitType.RQ: [CircuitType.RQ],
    GenericCircuitType.IPQ: [CircuitType.IPQ2, CircuitType.IPQ4, CircuitType.IPQ8],
    GenericCircuitType.IPD: [CircuitType.IPD2, CircuitType.IPD2_B1B2],
    GenericCircuitType.IT: [CircuitType.IT],
}


def arguments_check(
    circuit_type: GenericCircuitType,
    supported_types: Sequence[GenericCircuitType],
    start_time: int | str | datetime.datetime,
    stop_time: int | str | datetime.datetime,
) -> None:
    """Throws ValueError if the provided arguments are not consistent."""
    if Time.to_unix_timestamp(start_time) >= Time.to_unix_timestamp(stop_time):
        raise ValueError("stop_time must be strictly grater than start_time.")

    if circuit_type not in supported_types:
        raise ValueError(f"circuit type must be one of the {supported_types}")
