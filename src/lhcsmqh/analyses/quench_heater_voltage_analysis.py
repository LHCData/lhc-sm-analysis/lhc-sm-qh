"""Module containing the class to analyze QH events for IPD/IPQ/IT circuits."""

from __future__ import annotations

import datetime

import pandas as pd
from lhcsmapi.api import analysis
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import GenericCircuitType
from lhcsmapi.Time import Time

from lhcsmqh.analyses import commons


class QuenchHeaterVoltageAnalysis(analysis.Analysis):
    """Analysis class for QH IPD/IPQ/IT circuits."""

    def __init__(
        self,
        identifier: str,
        circuit_name: str,
        discharge_level: int,
        start_time: int | str | datetime.datetime,
        stop_time: int | str | datetime.datetime,
    ) -> None:
        """Instantiates a QuenchHeaterVoltageAnalysis class.

        Args:
            identifier: string identifying the analysis object
            circuit_name: circuit name to query for the analysis
            discharge_level: parameter of the analysis
            start_time: start time for the events query
            stop_time: stop time for the events query
        """
        self._circuit_type = signal_metadata.get_generic_circuit_type_for_circuit_name(circuit_name)
        commons.arguments_check(
            self._circuit_type,
            (GenericCircuitType.IPQ, GenericCircuitType.IPD, GenericCircuitType.IT),
            start_time,
            stop_time,
        )

        self._circuit_name = circuit_name
        self._start_time = Time.to_unix_timestamp(start_time)
        self._stop_time = Time.to_unix_timestamp(stop_time)
        self._discharge_level = discharge_level
        super().__init__(identifier)

    def search_discharges(self) -> pd.DataFrame:
        return commons.find_source_timestamp_qh(
            self._circuit_type, self._circuit_name, self._start_time, self._stop_time
        )

    def query(self, discharges: pd.DataFrame | None = None) -> None:
        self._events = discharges if discharges is not None else self.search_discharges()
        self.voltage_events: list[commons.VoltageEvent] = []
        for source, timestamp in self._events[["source", "timestamp"]].values:
            voltage_event = commons.query_voltage_event(self._circuit_type, self._circuit_name, source, timestamp)
            if voltage_event is None:
                self._logger.warning(
                    f"No Quench Heater Discharges in {source} at {Time.to_string_short(timestamp)}, skipping."
                )
            else:
                self.voltage_events.append(voltage_event)

    def analyze(self) -> None:
        self.voltage_results = [
            commons.analyze_voltage_event(event, self._discharge_level) for event in self.voltage_events
        ]

    def get_analysis_output(self) -> bool:
        return all(self.voltage_results)
