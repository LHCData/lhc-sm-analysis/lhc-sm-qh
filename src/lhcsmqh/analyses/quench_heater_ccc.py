"""Module containing the class used to analyze QH events in the CCC."""

from __future__ import annotations

import datetime

import pandas as pd
from lhcsmapi.api import analysis
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import GenericCircuitType
from lhcsmapi.Time import Time

from lhcsmqh.analyses import commons


class QHCCCAnalysis(analysis.Analysis):
    """QH Analysis for CCC"""

    _NOMINAL_VOLTAGE = 900
    _MEAN_START_VALUE = 50

    def __init__(
        self,
        identifier: str,
        start_time: int | str | datetime.datetime,
        stop_time: int | str | datetime.datetime,
        initial_charge_check: bool,
    ):
        """Creates a new QHCCCAnalysis instance.

        Args:
            identifier: Identifier of the analysis class that be used to retrieve the analysis from `AnalysisManager`
            start_time: Start time to query PM and NxCALS
            stop_time: Stop time to query PM and NxCALS
            initial_charge_check: If it is set to True, `max_charge` will be queried from NxCALS
                                  and only high discharge events will be analyzed
        """
        super().__init__(identifier)

        self._start_time = Time.to_unix_timestamp(start_time)

        self._stop_time = Time.to_unix_timestamp(stop_time)

        self._initial_charge_check = initial_charge_check

        if initial_charge_check:
            self._charge_check_level = 800

    @property
    def initial_charge_check(self) -> bool:
        """Returns whether if the initial charge check has been enabled."""
        return self._initial_charge_check

    def _get_qh_max_charge_from_nxcals(self, source: str, circuit_name: str, timestamp: int) -> float:
        meta_circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)
        source = f"MQ.{source}" if meta_circuit_type == "RQ" else f"MB.{source}"

        duration = int(60 * 1e9)
        t_start = timestamp - duration

        qh_spark = commons.query_single_qh_event_nxcals(
            self._spark, meta_circuit_type, circuit_name, source, t_start, duration, "U_HDS"
        )

        qh_max_charge = 0

        if qh_spark:
            qh_max_charge = max(df.mean()[0] for df in qh_spark)

        return qh_max_charge

    def query(self) -> None:
        self.low_charge_events = pd.DataFrame()
        self.voltage_events: list[commons.VoltageEvent] = []
        self.voltage_current_events: list[commons.VoltageCurrentEvent] = []
        for circuit_type, detailed_circuit_types in commons.DETAILED_CIRCUIT_TYPES_MAP.items():
            circuit_names = signal_metadata.get_circuit_names(list(detailed_circuit_types))

            if circuit_type == GenericCircuitType.RQ:
                circuit_names = circuit_names[0:7]  # RQFs and RQDs are the same from the powering point of view

            for circuit_name in circuit_names:
                meta_circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)
                source_timestamp_qds_df_i = commons.find_source_timestamp_qh(
                    meta_circuit_type, circuit_name, self._start_time, self._stop_time
                )

                if not source_timestamp_qds_df_i.empty:
                    for source, timestamp in source_timestamp_qds_df_i[["source", "timestamp"]].values:
                        charge_check = True
                        if self._initial_charge_check is True:
                            max_charge = self._get_qh_max_charge_from_nxcals(source, circuit_name, timestamp)
                            if max_charge < self._charge_check_level:
                                self.low_charge_events = pd.concat(
                                    (
                                        self.low_charge_events,
                                        pd.DataFrame(
                                            {"source": [source], "timestamp": [timestamp], "max_charge": [max_charge]}
                                        ),
                                    ),
                                    ignore_index=True,
                                )
                                charge_check = False

                        if charge_check is True:
                            if circuit_type in (GenericCircuitType.RB, GenericCircuitType.RQ):
                                current_offset = 0.085 if circuit_type == GenericCircuitType.RB else 0.025
                                self.voltage_current_events.append(
                                    commons.query_voltage_current_event(
                                        meta_circuit_type,
                                        circuit_name,
                                        source,
                                        timestamp,
                                        current_offset,
                                        self._MEAN_START_VALUE,
                                    )
                                )
                            else:
                                voltage_event = commons.query_voltage_event(
                                    meta_circuit_type, circuit_name, source, timestamp
                                )
                                if voltage_event is None:
                                    self._logger.warning(
                                        f"No Quench Heater Discharges in {source} at {Time.to_string_short(timestamp)},"
                                        " skipping."
                                    )
                                else:
                                    self.voltage_events.append(voltage_event)

    def analyze(self) -> None:
        self.voltage_results = [
            commons.analyze_voltage_event(event, self._NOMINAL_VOLTAGE) for event in self.voltage_events
        ]
        self.voltage_current_results = [
            commons.analyze_voltage_current_event(event, self._NOMINAL_VOLTAGE) for event in self.voltage_current_events
        ]
        self.events_number = len(self.voltage_current_events) + len(self.voltage_events)

    def get_analysis_output(self) -> bool:
        return all(self.voltage_current_results) and all(self.voltage_results)
