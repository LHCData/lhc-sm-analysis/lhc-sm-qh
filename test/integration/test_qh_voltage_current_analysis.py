import pandas as pd
import pytest
from numpy import nan

from lhcsmqh.analyses import QuenchHeaterVoltageCurrentAnalysis, VoltageCurrentResult


@pytest.mark.parametrize(
    ("circuit_name", "discharge_level", "t_start", "t_end", "analysis_result", "voltage_current_results"),
    [
        (
            "RB.A12",
            900,
            "2021-04-28 12:00:00+01:00",
            "2021-04-29 12:00:00+01:00",
            True,
            [
                VoltageCurrentResult(
                    source="B30R1",
                    timestamp=1619642471474000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "B30R1:U_HDS_1:first": 780.0,
                                "B30R1:U_HDS_1:last20mean": 15.0,
                                "B30R1:U_HDS_2:first": 780.0,
                                "B30R1:U_HDS_2:last20mean": 15.0,
                                "B30R1:U_HDS_3:first": 780.0,
                                "B30R1:U_HDS_3:last20mean": 15.0,
                                "B30R1:U_HDS_4:first": 780.0,
                                "B30R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "B30R1:U_HDS_1:first": 980.0,
                                "B30R1:U_HDS_1:last20mean": 85.0,
                                "B30R1:U_HDS_2:first": 980.0,
                                "B30R1:U_HDS_2:last20mean": 85.0,
                                "B30R1:U_HDS_3:first": 980.0,
                                "B30R1:U_HDS_3:last20mean": 85.0,
                                "B30R1:U_HDS_4:first": 980.0,
                                "B30R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "B30R1:U_HDS_1:first": 902.85516,
                                "B30R1:U_HDS_1:last20mean": 28.18751045,
                                "B30R1:U_HDS_2:first": 911.53735,
                                "B30R1:U_HDS_2:last20mean": 28.5919144,
                                "B30R1:U_HDS_3:first": 906.8225,
                                "B30R1:U_HDS_3:last20mean": 30.199945700000008,
                                "B30R1:U_HDS_4:first": 905.5959,
                                "B30R1:U_HDS_4:last20mean": 27.927810299999997,
                            },
                            "result": {
                                "B30R1:U_HDS_1:first": True,
                                "B30R1:U_HDS_1:last20mean": True,
                                "B30R1:U_HDS_2:first": True,
                                "B30R1:U_HDS_2:last20mean": True,
                                "B30R1:U_HDS_3:first": True,
                                "B30R1:U_HDS_3:last20mean": True,
                                "B30R1:U_HDS_4:first": True,
                                "B30R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B30R1:U_HDS_1:tau_charge": 0.08127265805820821,
                                "B30R1:U_HDS_2:tau_charge": 0.08096847543520991,
                                "B30R1:U_HDS_3:tau_charge": 0.08334965650487064,
                                "B30R1:U_HDS_4:tau_charge": 0.08070534444962309,
                                "B30R1:I_HDS_1:tau_charge": 0.07249529926259694,
                                "B30R1:I_HDS_2:tau_charge": 0.07224425062658714,
                                "B30R1:I_HDS_3:tau_charge": 0.07424427384477601,
                                "B30R1:I_HDS_4:tau_charge": 0.07249095069829457,
                            },
                            "act": {
                                "B30R1:U_HDS_1:tau_charge": 0.08141216372119284,
                                "B30R1:U_HDS_2:tau_charge": 0.08145068303631889,
                                "B30R1:U_HDS_3:tau_charge": 0.08351303118413768,
                                "B30R1:U_HDS_4:tau_charge": 0.08102357651293245,
                                "B30R1:I_HDS_1:tau_charge": 0.07255466760279725,
                                "B30R1:I_HDS_2:tau_charge": 0.07245070226869993,
                                "B30R1:I_HDS_3:tau_charge": 0.07437973063485026,
                                "B30R1:I_HDS_4:tau_charge": 0.07250881395420238,
                            },
                            "diff": {
                                "B30R1:U_HDS_1:tau_charge": 0.003,
                                "B30R1:U_HDS_2:tau_charge": 0.003,
                                "B30R1:U_HDS_3:tau_charge": 0.003,
                                "B30R1:U_HDS_4:tau_charge": 0.003,
                                "B30R1:I_HDS_1:tau_charge": 0.003,
                                "B30R1:I_HDS_2:tau_charge": 0.003,
                                "B30R1:I_HDS_3:tau_charge": 0.003,
                                "B30R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "B30R1:U_HDS_1:tau_charge": True,
                                "B30R1:U_HDS_2:tau_charge": True,
                                "B30R1:U_HDS_3:tau_charge": True,
                                "B30R1:U_HDS_4:tau_charge": True,
                                "B30R1:I_HDS_1:tau_charge": True,
                                "B30R1:I_HDS_2:tau_charge": True,
                                "B30R1:I_HDS_3:tau_charge": True,
                                "B30R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B30R1:R_HDS_1:first20mean": 10.9060651443099,
                                "B30R1:R_HDS_2:first20mean": 10.83723578626863,
                                "B30R1:R_HDS_3:first20mean": 10.930038920226862,
                                "B30R1:R_HDS_4:first20mean": 11.045098218936065,
                            },
                            "act": {
                                "B30R1:R_HDS_1:first20mean": 10.965732672000172,
                                "B30R1:R_HDS_2:first20mean": 10.905434722266879,
                                "B30R1:R_HDS_3:first20mean": 10.984272815727616,
                                "B30R1:R_HDS_4:first20mean": 11.100794941960109,
                            },
                            "diff": {
                                "B30R1:R_HDS_1:first20mean": 0.5,
                                "B30R1:R_HDS_2:first20mean": 0.5,
                                "B30R1:R_HDS_3:first20mean": 0.5,
                                "B30R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "B30R1:R_HDS_1:first20mean": True,
                                "B30R1:R_HDS_2:first20mean": True,
                                "B30R1:R_HDS_3:first20mean": True,
                                "B30R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B30R1_C_HDS_1:capacitance": 0.007452060572058033,
                                "B30R1_C_HDS_2:capacitance": 0.007471321749573945,
                                "B30R1_C_HDS_3:capacitance": 0.007625741967910637,
                                "B30R1_C_HDS_4:capacitance": 0.007306892419594721,
                            },
                            "act": {
                                "B30R1_C_HDS_1:capacitance": 0.007424233852524064,
                                "B30R1_C_HDS_2:capacitance": 0.00746881578870136,
                                "B30R1_C_HDS_3:capacitance": 0.007602964036414061,
                                "B30R1_C_HDS_4:capacitance": 0.007298898586683181,
                            },
                            "diff": {
                                "B30R1_C_HDS_1:capacitance": nan,
                                "B30R1_C_HDS_2:capacitance": nan,
                                "B30R1_C_HDS_3:capacitance": nan,
                                "B30R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "B30R1_C_HDS_1:capacitance": nan,
                                "B30R1_C_HDS_2:capacitance": nan,
                                "B30R1_C_HDS_3:capacitance": nan,
                                "B30R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="C30R1",
                    timestamp=1619642504413000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "C30R1:U_HDS_1:first": 780.0,
                                "C30R1:U_HDS_1:last20mean": 15.0,
                                "C30R1:U_HDS_2:first": 780.0,
                                "C30R1:U_HDS_2:last20mean": 15.0,
                                "C30R1:U_HDS_3:first": 780.0,
                                "C30R1:U_HDS_3:last20mean": 15.0,
                                "C30R1:U_HDS_4:first": 780.0,
                                "C30R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "C30R1:U_HDS_1:first": 980.0,
                                "C30R1:U_HDS_1:last20mean": 85.0,
                                "C30R1:U_HDS_2:first": 980.0,
                                "C30R1:U_HDS_2:last20mean": 85.0,
                                "C30R1:U_HDS_3:first": 980.0,
                                "C30R1:U_HDS_3:last20mean": 85.0,
                                "C30R1:U_HDS_4:first": 980.0,
                                "C30R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "C30R1:U_HDS_1:first": 906.4584,
                                "C30R1:U_HDS_1:last20mean": 44.998053350000006,
                                "C30R1:U_HDS_2:first": 924.2636,
                                "C30R1:U_HDS_2:last20mean": 41.890278349999996,
                                "C30R1:U_HDS_3:first": 902.81683,
                                "C30R1:U_HDS_3:last20mean": 44.747936499999994,
                                "C30R1:U_HDS_4:first": 924.1678,
                                "C30R1:U_HDS_4:last20mean": 44.70768805000001,
                            },
                            "result": {
                                "C30R1:U_HDS_1:first": True,
                                "C30R1:U_HDS_1:last20mean": True,
                                "C30R1:U_HDS_2:first": True,
                                "C30R1:U_HDS_2:last20mean": True,
                                "C30R1:U_HDS_3:first": True,
                                "C30R1:U_HDS_3:last20mean": True,
                                "C30R1:U_HDS_4:first": True,
                                "C30R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C30R1:U_HDS_1:tau_charge": 0.09737034361696614,
                                "C30R1:U_HDS_2:tau_charge": 0.09385962842905504,
                                "C30R1:U_HDS_3:tau_charge": 0.09792605701204238,
                                "C30R1:U_HDS_4:tau_charge": 0.09636102034854202,
                                "C30R1:I_HDS_1:tau_charge": 0.0862895048690526,
                                "C30R1:I_HDS_2:tau_charge": 0.08345088601638612,
                                "C30R1:I_HDS_3:tau_charge": 0.08689560859481917,
                                "C30R1:I_HDS_4:tau_charge": 0.08537628630544906,
                            },
                            "act": {
                                "C30R1:U_HDS_1:tau_charge": 0.09616146220827065,
                                "C30R1:U_HDS_2:tau_charge": 0.09289309615385268,
                                "C30R1:U_HDS_3:tau_charge": 0.09664437425558359,
                                "C30R1:U_HDS_4:tau_charge": 0.09550181087397178,
                                "C30R1:I_HDS_1:tau_charge": 0.08562760455985721,
                                "C30R1:I_HDS_2:tau_charge": 0.08270537345986989,
                                "C30R1:I_HDS_3:tau_charge": 0.08615680274937135,
                                "C30R1:I_HDS_4:tau_charge": 0.08481123545996125,
                            },
                            "diff": {
                                "C30R1:U_HDS_1:tau_charge": 0.003,
                                "C30R1:U_HDS_2:tau_charge": 0.003,
                                "C30R1:U_HDS_3:tau_charge": 0.003,
                                "C30R1:U_HDS_4:tau_charge": 0.003,
                                "C30R1:I_HDS_1:tau_charge": 0.003,
                                "C30R1:I_HDS_2:tau_charge": 0.003,
                                "C30R1:I_HDS_3:tau_charge": 0.003,
                                "C30R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "C30R1:U_HDS_1:tau_charge": True,
                                "C30R1:U_HDS_2:tau_charge": True,
                                "C30R1:U_HDS_3:tau_charge": True,
                                "C30R1:U_HDS_4:tau_charge": True,
                                "C30R1:I_HDS_1:tau_charge": True,
                                "C30R1:I_HDS_2:tau_charge": True,
                                "C30R1:I_HDS_3:tau_charge": True,
                                "C30R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C30R1:R_HDS_1:first20mean": 11.36602536416508,
                                "C30R1:R_HDS_2:first20mean": 10.947699136071957,
                                "C30R1:R_HDS_3:first20mean": 11.390347579879514,
                                "C30R1:R_HDS_4:first20mean": 10.94255622092463,
                            },
                            "act": {
                                "C30R1:R_HDS_1:first20mean": 11.36083641738078,
                                "C30R1:R_HDS_2:first20mean": 10.928934924857604,
                                "C30R1:R_HDS_3:first20mean": 11.379955754497779,
                                "C30R1:R_HDS_4:first20mean": 10.926656361268535,
                            },
                            "diff": {
                                "C30R1:R_HDS_1:first20mean": 0.5,
                                "C30R1:R_HDS_2:first20mean": 0.5,
                                "C30R1:R_HDS_3:first20mean": 0.5,
                                "C30R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "C30R1:R_HDS_1:first20mean": True,
                                "C30R1:R_HDS_2:first20mean": True,
                                "C30R1:R_HDS_3:first20mean": True,
                                "C30R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C30R1_C_HDS_1:capacitance": 0.008566789224662153,
                                "C30R1_C_HDS_2:capacitance": 0.008573457058186197,
                                "C30R1_C_HDS_3:capacitance": 0.00859728435197394,
                                "C30R1_C_HDS_4:capacitance": 0.008806079530510253,
                            },
                            "act": {
                                "C30R1_C_HDS_1:capacitance": 0.008464294236395713,
                                "C30R1_C_HDS_2:capacitance": 0.008499739159629318,
                                "C30R1_C_HDS_3:capacitance": 0.008492508788304046,
                                "C30R1_C_HDS_4:capacitance": 0.008740259391015063,
                            },
                            "diff": {
                                "C30R1_C_HDS_1:capacitance": nan,
                                "C30R1_C_HDS_2:capacitance": nan,
                                "C30R1_C_HDS_3:capacitance": nan,
                                "C30R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "C30R1_C_HDS_1:capacitance": nan,
                                "C30R1_C_HDS_2:capacitance": nan,
                                "C30R1_C_HDS_3:capacitance": nan,
                                "C30R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="A30R1",
                    timestamp=1619642504845000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "A30R1:U_HDS_1:first": 780.0,
                                "A30R1:U_HDS_1:last20mean": 15.0,
                                "A30R1:U_HDS_2:first": 780.0,
                                "A30R1:U_HDS_2:last20mean": 15.0,
                                "A30R1:U_HDS_3:first": 780.0,
                                "A30R1:U_HDS_3:last20mean": 15.0,
                                "A30R1:U_HDS_4:first": 780.0,
                                "A30R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "A30R1:U_HDS_1:first": 980.0,
                                "A30R1:U_HDS_1:last20mean": 85.0,
                                "A30R1:U_HDS_2:first": 980.0,
                                "A30R1:U_HDS_2:last20mean": 85.0,
                                "A30R1:U_HDS_3:first": 980.0,
                                "A30R1:U_HDS_3:last20mean": 85.0,
                                "A30R1:U_HDS_4:first": 980.0,
                                "A30R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "A30R1:U_HDS_1:first": 903.3726,
                                "A30R1:U_HDS_1:last20mean": 31.24737065,
                                "A30R1:U_HDS_2:first": 907.7425,
                                "A30R1:U_HDS_2:last20mean": 30.904298200000007,
                                "A30R1:U_HDS_3:first": 902.45264,
                                "A30R1:U_HDS_3:last20mean": 27.727524799999998,
                                "A30R1:U_HDS_4:first": 903.20013,
                                "A30R1:U_HDS_4:last20mean": 30.245944199999997,
                            },
                            "result": {
                                "A30R1:U_HDS_1:first": True,
                                "A30R1:U_HDS_1:last20mean": True,
                                "A30R1:U_HDS_2:first": True,
                                "A30R1:U_HDS_2:last20mean": True,
                                "A30R1:U_HDS_3:first": True,
                                "A30R1:U_HDS_3:last20mean": True,
                                "A30R1:U_HDS_4:first": True,
                                "A30R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A30R1:U_HDS_1:tau_charge": 0.0852472882275096,
                                "A30R1:U_HDS_2:tau_charge": 0.08462046917405441,
                                "A30R1:U_HDS_3:tau_charge": 0.08182370821683485,
                                "A30R1:U_HDS_4:tau_charge": 0.08426855256321997,
                                "A30R1:I_HDS_1:tau_charge": 0.07594135113203274,
                                "A30R1:I_HDS_2:tau_charge": 0.07555482967045676,
                                "A30R1:I_HDS_3:tau_charge": 0.07261946633994182,
                                "A30R1:I_HDS_4:tau_charge": 0.07529708954336106,
                            },
                            "act": {
                                "A30R1:U_HDS_1:tau_charge": 0.08505896630534233,
                                "A30R1:U_HDS_2:tau_charge": 0.08473679116531219,
                                "A30R1:U_HDS_3:tau_charge": 0.08169675118515174,
                                "A30R1:U_HDS_4:tau_charge": 0.08437281116624709,
                                "A30R1:I_HDS_1:tau_charge": 0.07620006874610748,
                                "A30R1:I_HDS_2:tau_charge": 0.07574760558372251,
                                "A30R1:I_HDS_3:tau_charge": 0.07291812644937042,
                                "A30R1:I_HDS_4:tau_charge": 0.07557656672302046,
                            },
                            "diff": {
                                "A30R1:U_HDS_1:tau_charge": 0.003,
                                "A30R1:U_HDS_2:tau_charge": 0.003,
                                "A30R1:U_HDS_3:tau_charge": 0.003,
                                "A30R1:U_HDS_4:tau_charge": 0.003,
                                "A30R1:I_HDS_1:tau_charge": 0.003,
                                "A30R1:I_HDS_2:tau_charge": 0.003,
                                "A30R1:I_HDS_3:tau_charge": 0.003,
                                "A30R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "A30R1:U_HDS_1:tau_charge": True,
                                "A30R1:U_HDS_2:tau_charge": True,
                                "A30R1:U_HDS_3:tau_charge": True,
                                "A30R1:U_HDS_4:tau_charge": True,
                                "A30R1:I_HDS_1:tau_charge": True,
                                "A30R1:I_HDS_2:tau_charge": True,
                                "A30R1:I_HDS_3:tau_charge": True,
                                "A30R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A30R1:R_HDS_1:first20mean": 10.959936529080192,
                                "A30R1:R_HDS_2:first20mean": 11.070784818614282,
                                "A30R1:R_HDS_3:first20mean": 10.553141931653423,
                                "A30R1:R_HDS_4:first20mean": 10.984638495290154,
                            },
                            "act": {
                                "A30R1:R_HDS_1:first20mean": 11.021683066495847,
                                "A30R1:R_HDS_2:first20mean": 11.134045454354176,
                                "A30R1:R_HDS_3:first20mean": 10.622370519459402,
                                "A30R1:R_HDS_4:first20mean": 11.053591694584672,
                            },
                            "diff": {
                                "A30R1:R_HDS_1:first20mean": 0.5,
                                "A30R1:R_HDS_2:first20mean": 0.5,
                                "A30R1:R_HDS_3:first20mean": 0.5,
                                "A30R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "A30R1:R_HDS_1:first20mean": True,
                                "A30R1:R_HDS_2:first20mean": True,
                                "A30R1:R_HDS_3:first20mean": True,
                                "A30R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A30R1_C_HDS_1:capacitance": 0.007778082291017058,
                                "A30R1_C_HDS_2:capacitance": 0.007643583590548576,
                                "A30R1_C_HDS_3:capacitance": 0.007753492632503148,
                                "A30R1_C_HDS_4:capacitance": 0.007671490745858546,
                            },
                            "act": {
                                "A30R1_C_HDS_1:capacitance": 0.007717420814240974,
                                "A30R1_C_HDS_2:capacitance": 0.007610602230133189,
                                "A30R1_C_HDS_3:capacitance": 0.007691009368905867,
                                "A30R1_C_HDS_4:capacitance": 0.0076330674677971555,
                            },
                            "diff": {
                                "A30R1_C_HDS_1:capacitance": nan,
                                "A30R1_C_HDS_2:capacitance": nan,
                                "A30R1_C_HDS_3:capacitance": nan,
                                "A30R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "A30R1_C_HDS_1:capacitance": nan,
                                "A30R1_C_HDS_2:capacitance": nan,
                                "A30R1_C_HDS_3:capacitance": nan,
                                "A30R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="A31R1",
                    timestamp=1619642594949000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "A31R1:U_HDS_1:first": 780.0,
                                "A31R1:U_HDS_1:last20mean": 15.0,
                                "A31R1:U_HDS_2:first": 780.0,
                                "A31R1:U_HDS_2:last20mean": 15.0,
                                "A31R1:U_HDS_3:first": 780.0,
                                "A31R1:U_HDS_3:last20mean": 15.0,
                                "A31R1:U_HDS_4:first": 780.0,
                                "A31R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "A31R1:U_HDS_1:first": 980.0,
                                "A31R1:U_HDS_1:last20mean": 85.0,
                                "A31R1:U_HDS_2:first": 980.0,
                                "A31R1:U_HDS_2:last20mean": 85.0,
                                "A31R1:U_HDS_3:first": 980.0,
                                "A31R1:U_HDS_3:last20mean": 85.0,
                                "A31R1:U_HDS_4:first": 980.0,
                                "A31R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "A31R1:U_HDS_1:first": 910.9241,
                                "A31R1:U_HDS_1:last20mean": 25.47072315,
                                "A31R1:U_HDS_2:first": 905.3659,
                                "A31R1:U_HDS_2:last20mean": 25.319311000000003,
                                "A31R1:U_HDS_3:first": 910.19574,
                                "A31R1:U_HDS_3:last20mean": 25.323144250000002,
                                "A31R1:U_HDS_4:first": 905.82587,
                                "A31R1:U_HDS_4:last20mean": 25.232105400000002,
                            },
                            "result": {
                                "A31R1:U_HDS_1:first": True,
                                "A31R1:U_HDS_1:last20mean": True,
                                "A31R1:U_HDS_2:first": True,
                                "A31R1:U_HDS_2:last20mean": True,
                                "A31R1:U_HDS_3:first": True,
                                "A31R1:U_HDS_3:last20mean": True,
                                "A31R1:U_HDS_4:first": True,
                                "A31R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A31R1:U_HDS_1:tau_charge": 0.08205860177944559,
                                "A31R1:U_HDS_2:tau_charge": 0.08159715351200703,
                                "A31R1:U_HDS_3:tau_charge": 0.08176324104091667,
                                "A31R1:U_HDS_4:tau_charge": 0.08167254539439248,
                                "A31R1:I_HDS_1:tau_charge": 0.07328818428065022,
                                "A31R1:I_HDS_2:tau_charge": 0.07291446470427711,
                                "A31R1:I_HDS_3:tau_charge": 0.07277088757642759,
                                "A31R1:I_HDS_4:tau_charge": 0.07296195002870062,
                            },
                            "act": {
                                "A31R1:U_HDS_1:tau_charge": 0.08138249359186525,
                                "A31R1:U_HDS_2:tau_charge": 0.08118830221396872,
                                "A31R1:U_HDS_3:tau_charge": 0.08110107207904903,
                                "A31R1:U_HDS_4:tau_charge": 0.08128503327951826,
                                "A31R1:I_HDS_1:tau_charge": 0.07272281508718076,
                                "A31R1:I_HDS_2:tau_charge": 0.072387998348359,
                                "A31R1:I_HDS_3:tau_charge": 0.07234833182155528,
                                "A31R1:I_HDS_4:tau_charge": 0.07256170048737913,
                            },
                            "diff": {
                                "A31R1:U_HDS_1:tau_charge": 0.003,
                                "A31R1:U_HDS_2:tau_charge": 0.003,
                                "A31R1:U_HDS_3:tau_charge": 0.003,
                                "A31R1:U_HDS_4:tau_charge": 0.003,
                                "A31R1:I_HDS_1:tau_charge": 0.003,
                                "A31R1:I_HDS_2:tau_charge": 0.003,
                                "A31R1:I_HDS_3:tau_charge": 0.003,
                                "A31R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "A31R1:U_HDS_1:tau_charge": True,
                                "A31R1:U_HDS_2:tau_charge": True,
                                "A31R1:U_HDS_3:tau_charge": True,
                                "A31R1:U_HDS_4:tau_charge": True,
                                "A31R1:I_HDS_1:tau_charge": True,
                                "A31R1:I_HDS_2:tau_charge": True,
                                "A31R1:I_HDS_3:tau_charge": True,
                                "A31R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A31R1:R_HDS_1:first20mean": 10.881830988944525,
                                "A31R1:R_HDS_2:first20mean": 10.755261932667477,
                                "A31R1:R_HDS_3:first20mean": 10.76671985097791,
                                "A31R1:R_HDS_4:first20mean": 10.860381086228752,
                            },
                            "act": {
                                "A31R1:R_HDS_1:first20mean": 10.80004328360667,
                                "A31R1:R_HDS_2:first20mean": 10.679488374413825,
                                "A31R1:R_HDS_3:first20mean": 10.688022772164802,
                                "A31R1:R_HDS_4:first20mean": 10.786840609899146,
                            },
                            "diff": {
                                "A31R1:R_HDS_1:first20mean": 0.5,
                                "A31R1:R_HDS_2:first20mean": 0.5,
                                "A31R1:R_HDS_3:first20mean": 0.5,
                                "A31R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "A31R1:R_HDS_1:first20mean": True,
                                "A31R1:R_HDS_2:first20mean": True,
                                "A31R1:R_HDS_3:first20mean": True,
                                "A31R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A31R1_C_HDS_1:capacitance": 0.007540881848175515,
                                "A31R1_C_HDS_2:capacitance": 0.007586719321467016,
                                "A31R1_C_HDS_3:capacitance": 0.007594071562425798,
                                "A31R1_C_HDS_4:capacitance": 0.007520228318503059,
                            },
                            "act": {
                                "A31R1_C_HDS_1:capacitance": 0.007535385873443241,
                                "A31R1_C_HDS_2:capacitance": 0.007602265143008311,
                                "A31R1_C_HDS_3:capacitance": 0.007588033241308527,
                                "A31R1_C_HDS_4:capacitance": 0.007535573780975545,
                            },
                            "diff": {
                                "A31R1_C_HDS_1:capacitance": nan,
                                "A31R1_C_HDS_2:capacitance": nan,
                                "A31R1_C_HDS_3:capacitance": nan,
                                "A31R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "A31R1_C_HDS_1:capacitance": nan,
                                "A31R1_C_HDS_2:capacitance": nan,
                                "A31R1_C_HDS_3:capacitance": nan,
                                "A31R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="B31R1",
                    timestamp=1619642820513000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "B31R1:U_HDS_1:first": 780.0,
                                "B31R1:U_HDS_1:last20mean": 15.0,
                                "B31R1:U_HDS_2:first": 780.0,
                                "B31R1:U_HDS_2:last20mean": 15.0,
                                "B31R1:U_HDS_3:first": 780.0,
                                "B31R1:U_HDS_3:last20mean": 15.0,
                                "B31R1:U_HDS_4:first": 780.0,
                                "B31R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "B31R1:U_HDS_1:first": 980.0,
                                "B31R1:U_HDS_1:last20mean": 85.0,
                                "B31R1:U_HDS_2:first": 980.0,
                                "B31R1:U_HDS_2:last20mean": 85.0,
                                "B31R1:U_HDS_3:first": 980.0,
                                "B31R1:U_HDS_3:last20mean": 85.0,
                                "B31R1:U_HDS_4:first": 980.0,
                                "B31R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "B31R1:U_HDS_1:first": 880.9675,
                                "B31R1:U_HDS_1:last20mean": 25.4266412,
                                "B31R1:U_HDS_2:first": 878.2076,
                                "B31R1:U_HDS_2:last20mean": 24.41371525,
                                "B31R1:U_HDS_3:first": 885.6057,
                                "B31R1:U_HDS_3:last20mean": 25.166941,
                                "B31R1:U_HDS_4:first": 879.8559,
                                "B31R1:U_HDS_4:last20mean": 25.988205900000004,
                            },
                            "result": {
                                "B31R1:U_HDS_1:first": True,
                                "B31R1:U_HDS_1:last20mean": True,
                                "B31R1:U_HDS_2:first": True,
                                "B31R1:U_HDS_2:last20mean": True,
                                "B31R1:U_HDS_3:first": True,
                                "B31R1:U_HDS_3:last20mean": True,
                                "B31R1:U_HDS_4:first": True,
                                "B31R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B31R1:U_HDS_1:tau_charge": 0.08210870964057362,
                                "B31R1:U_HDS_2:tau_charge": 0.08134467358300469,
                                "B31R1:U_HDS_3:tau_charge": 0.08217759399404458,
                                "B31R1:U_HDS_4:tau_charge": 0.08296061549424175,
                                "B31R1:I_HDS_1:tau_charge": 0.07336632377782903,
                                "B31R1:I_HDS_2:tau_charge": 0.0726907215587968,
                                "B31R1:I_HDS_3:tau_charge": 0.07349283369957343,
                                "B31R1:I_HDS_4:tau_charge": 0.07409855322155497,
                            },
                            "act": {
                                "B31R1:U_HDS_1:tau_charge": 0.08125287124585107,
                                "B31R1:U_HDS_2:tau_charge": 0.08074007469507442,
                                "B31R1:U_HDS_3:tau_charge": 0.08137968025107895,
                                "B31R1:U_HDS_4:tau_charge": 0.08224639014714155,
                                "B31R1:I_HDS_1:tau_charge": 0.072606731327623,
                                "B31R1:I_HDS_2:tau_charge": 0.07210169754225021,
                                "B31R1:I_HDS_3:tau_charge": 0.0727153248178209,
                                "B31R1:I_HDS_4:tau_charge": 0.07337588335517747,
                            },
                            "diff": {
                                "B31R1:U_HDS_1:tau_charge": 0.003,
                                "B31R1:U_HDS_2:tau_charge": 0.003,
                                "B31R1:U_HDS_3:tau_charge": 0.003,
                                "B31R1:U_HDS_4:tau_charge": 0.003,
                                "B31R1:I_HDS_1:tau_charge": 0.003,
                                "B31R1:I_HDS_2:tau_charge": 0.003,
                                "B31R1:I_HDS_3:tau_charge": 0.003,
                                "B31R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "B31R1:U_HDS_1:tau_charge": True,
                                "B31R1:U_HDS_2:tau_charge": True,
                                "B31R1:U_HDS_3:tau_charge": True,
                                "B31R1:U_HDS_4:tau_charge": True,
                                "B31R1:I_HDS_1:tau_charge": True,
                                "B31R1:I_HDS_2:tau_charge": True,
                                "B31R1:I_HDS_3:tau_charge": True,
                                "B31R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B31R1:R_HDS_1:first20mean": 10.625758528115945,
                                "B31R1:R_HDS_2:first20mean": 10.529137083962471,
                                "B31R1:R_HDS_3:first20mean": 10.621652327264101,
                                "B31R1:R_HDS_4:first20mean": 10.622691076744236,
                            },
                            "act": {
                                "B31R1:R_HDS_1:first20mean": 10.549113016652665,
                                "B31R1:R_HDS_2:first20mean": 10.454707230806937,
                                "B31R1:R_HDS_3:first20mean": 10.54459734449943,
                                "B31R1:R_HDS_4:first20mean": 10.547695099672596,
                            },
                            "diff": {
                                "B31R1:R_HDS_1:first20mean": 0.5,
                                "B31R1:R_HDS_2:first20mean": 0.5,
                                "B31R1:R_HDS_3:first20mean": 0.5,
                                "B31R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "B31R1:R_HDS_1:first20mean": True,
                                "B31R1:R_HDS_2:first20mean": True,
                                "B31R1:R_HDS_3:first20mean": True,
                                "B31R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B31R1_C_HDS_1:capacitance": 0.007727326893728342,
                                "B31R1_C_HDS_2:capacitance": 0.007725673332423927,
                                "B31R1_C_HDS_3:capacitance": 0.007736799460391647,
                                "B31R1_C_HDS_4:capacitance": 0.007809755070055983,
                            },
                            "act": {
                                "B31R1_C_HDS_1:capacitance": 0.007702341525546892,
                                "B31R1_C_HDS_2:capacitance": 0.007722844161255635,
                                "B31R1_C_HDS_3:capacitance": 0.007717665984991879,
                                "B31R1_C_HDS_4:capacitance": 0.007797569930675613,
                            },
                            "diff": {
                                "B31R1_C_HDS_1:capacitance": nan,
                                "B31R1_C_HDS_2:capacitance": nan,
                                "B31R1_C_HDS_3:capacitance": nan,
                                "B31R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "B31R1_C_HDS_1:capacitance": nan,
                                "B31R1_C_HDS_2:capacitance": nan,
                                "B31R1_C_HDS_3:capacitance": nan,
                                "B31R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="A12R1",
                    timestamp=1619674434981000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "A12R1:U_HDS_1:first": 780.0,
                                "A12R1:U_HDS_1:last20mean": 15.0,
                                "A12R1:U_HDS_2:first": 780.0,
                                "A12R1:U_HDS_2:last20mean": 15.0,
                                "A12R1:U_HDS_3:first": 780.0,
                                "A12R1:U_HDS_3:last20mean": 15.0,
                                "A12R1:U_HDS_4:first": 780.0,
                                "A12R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "A12R1:U_HDS_1:first": 980.0,
                                "A12R1:U_HDS_1:last20mean": 85.0,
                                "A12R1:U_HDS_2:first": 980.0,
                                "A12R1:U_HDS_2:last20mean": 85.0,
                                "A12R1:U_HDS_3:first": 980.0,
                                "A12R1:U_HDS_3:last20mean": 85.0,
                                "A12R1:U_HDS_4:first": 980.0,
                                "A12R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "A12R1:U_HDS_1:first": 908.7583,
                                "A12R1:U_HDS_1:last20mean": 29.505176499999997,
                                "A12R1:U_HDS_2:first": 897.66113,
                                "A12R1:U_HDS_2:last20mean": 27.198542349999997,
                                "A12R1:U_HDS_3:first": 912.39984,
                                "A12R1:U_HDS_3:last20mean": 28.315923000000005,
                                "A12R1:U_HDS_4:first": 900.7277,
                                "A12R1:U_HDS_4:last20mean": 27.50328245,
                            },
                            "result": {
                                "A12R1:U_HDS_1:first": True,
                                "A12R1:U_HDS_1:last20mean": True,
                                "A12R1:U_HDS_2:first": True,
                                "A12R1:U_HDS_2:last20mean": True,
                                "A12R1:U_HDS_3:first": True,
                                "A12R1:U_HDS_3:last20mean": True,
                                "A12R1:U_HDS_4:first": True,
                                "A12R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A12R1:U_HDS_1:tau_charge": 0.08174731260371886,
                                "A12R1:U_HDS_2:tau_charge": 0.07951824950641183,
                                "A12R1:U_HDS_3:tau_charge": 0.08041672339035658,
                                "A12R1:U_HDS_4:tau_charge": 0.0796653041516371,
                                "A12R1:I_HDS_1:tau_charge": 0.072246836667893,
                                "A12R1:I_HDS_2:tau_charge": 0.07070390088363909,
                                "A12R1:I_HDS_3:tau_charge": 0.07146199048424699,
                                "A12R1:I_HDS_4:tau_charge": 0.07098524452851068,
                            },
                            "act": {
                                "A12R1:U_HDS_1:tau_charge": 0.08227543664519914,
                                "A12R1:U_HDS_2:tau_charge": 0.0801728546711485,
                                "A12R1:U_HDS_3:tau_charge": 0.08091245654733495,
                                "A12R1:U_HDS_4:tau_charge": 0.08034557088219944,
                                "A12R1:I_HDS_1:tau_charge": 0.07262730081967982,
                                "A12R1:I_HDS_2:tau_charge": 0.07106431595871598,
                                "A12R1:I_HDS_3:tau_charge": 0.07171144450158058,
                                "A12R1:I_HDS_4:tau_charge": 0.07130821590192599,
                            },
                            "diff": {
                                "A12R1:U_HDS_1:tau_charge": 0.003,
                                "A12R1:U_HDS_2:tau_charge": 0.003,
                                "A12R1:U_HDS_3:tau_charge": 0.003,
                                "A12R1:U_HDS_4:tau_charge": 0.003,
                                "A12R1:I_HDS_1:tau_charge": 0.003,
                                "A12R1:I_HDS_2:tau_charge": 0.003,
                                "A12R1:I_HDS_3:tau_charge": 0.003,
                                "A12R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "A12R1:U_HDS_1:tau_charge": True,
                                "A12R1:U_HDS_2:tau_charge": True,
                                "A12R1:U_HDS_3:tau_charge": True,
                                "A12R1:U_HDS_4:tau_charge": True,
                                "A12R1:I_HDS_1:tau_charge": True,
                                "A12R1:I_HDS_2:tau_charge": True,
                                "A12R1:I_HDS_3:tau_charge": True,
                                "A12R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A12R1:R_HDS_1:first20mean": 10.580362132069691,
                                "A12R1:R_HDS_2:first20mean": 10.532158067349936,
                                "A12R1:R_HDS_3:first20mean": 10.578098363727033,
                                "A12R1:R_HDS_4:first20mean": 10.584185778343034,
                            },
                            "act": {
                                "A12R1:R_HDS_1:first20mean": 10.659398621491224,
                                "A12R1:R_HDS_2:first20mean": 10.612162715299563,
                                "A12R1:R_HDS_3:first20mean": 10.658547693003829,
                                "A12R1:R_HDS_4:first20mean": 10.666158661313613,
                            },
                            "diff": {
                                "A12R1:R_HDS_1:first20mean": 0.5,
                                "A12R1:R_HDS_2:first20mean": 0.5,
                                "A12R1:R_HDS_3:first20mean": 0.5,
                                "A12R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "A12R1:R_HDS_1:first20mean": True,
                                "A12R1:R_HDS_2:first20mean": True,
                                "A12R1:R_HDS_3:first20mean": True,
                                "A12R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A12R1_C_HDS_1:capacitance": 0.007726324636463814,
                                "A12R1_C_HDS_2:capacitance": 0.007550043305267248,
                                "A12R1_C_HDS_3:capacitance": 0.007602190925555258,
                                "A12R1_C_HDS_4:capacitance": 0.007526824058081564,
                            },
                            "act": {
                                "A12R1_C_HDS_1:capacitance": 0.007718581466623959,
                                "A12R1_C_HDS_2:capacitance": 0.007554808272545919,
                                "A12R1_C_HDS_3:capacitance": 0.007591320963966332,
                                "A12R1_C_HDS_4:capacitance": 0.007532756021492026,
                            },
                            "diff": {
                                "A12R1_C_HDS_1:capacitance": nan,
                                "A12R1_C_HDS_2:capacitance": nan,
                                "A12R1_C_HDS_3:capacitance": nan,
                                "A12R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "A12R1_C_HDS_1:capacitance": nan,
                                "A12R1_C_HDS_2:capacitance": nan,
                                "A12R1_C_HDS_3:capacitance": nan,
                                "A12R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="B12R1",
                    timestamp=1619674460377000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "B12R1:U_HDS_1:first": 780.0,
                                "B12R1:U_HDS_1:last20mean": 15.0,
                                "B12R1:U_HDS_2:first": 780.0,
                                "B12R1:U_HDS_2:last20mean": 15.0,
                                "B12R1:U_HDS_3:first": 780.0,
                                "B12R1:U_HDS_3:last20mean": 15.0,
                                "B12R1:U_HDS_4:first": 780.0,
                                "B12R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "B12R1:U_HDS_1:first": 980.0,
                                "B12R1:U_HDS_1:last20mean": 85.0,
                                "B12R1:U_HDS_2:first": 980.0,
                                "B12R1:U_HDS_2:last20mean": 85.0,
                                "B12R1:U_HDS_3:first": 980.0,
                                "B12R1:U_HDS_3:last20mean": 85.0,
                                "B12R1:U_HDS_4:first": 980.0,
                                "B12R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "B12R1:U_HDS_1:first": 898.5428,
                                "B12R1:U_HDS_1:last20mean": 35.29332389999999,
                                "B12R1:U_HDS_2:first": 903.89014,
                                "B12R1:U_HDS_2:last20mean": 34.267939999999996,
                                "B12R1:U_HDS_3:first": 894.42206,
                                "B12R1:U_HDS_3:last20mean": 46.3329692,
                                "B12R1:U_HDS_4:first": 906.2092,
                                "B12R1:U_HDS_4:last20mean": 49.38612080000001,
                            },
                            "result": {
                                "B12R1:U_HDS_1:first": True,
                                "B12R1:U_HDS_1:last20mean": True,
                                "B12R1:U_HDS_2:first": True,
                                "B12R1:U_HDS_2:last20mean": True,
                                "B12R1:U_HDS_3:first": True,
                                "B12R1:U_HDS_3:last20mean": True,
                                "B12R1:U_HDS_4:first": True,
                                "B12R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B12R1:U_HDS_1:tau_charge": 0.08927385735320156,
                                "B12R1:U_HDS_2:tau_charge": 0.08838089260078767,
                                "B12R1:U_HDS_3:tau_charge": 0.09925251154866772,
                                "B12R1:U_HDS_4:tau_charge": 0.10116899475984698,
                                "B12R1:I_HDS_1:tau_charge": 0.07978056017059484,
                                "B12R1:I_HDS_2:tau_charge": 0.07929014362624161,
                                "B12R1:I_HDS_3:tau_charge": 0.09066738561455105,
                                "B12R1:I_HDS_4:tau_charge": 0.09239533099741676,
                            },
                            "act": {
                                "B12R1:U_HDS_1:tau_charge": 0.08880238188606901,
                                "B12R1:U_HDS_2:tau_charge": 0.08807332639874534,
                                "B12R1:U_HDS_3:tau_charge": 0.09875547030959918,
                                "B12R1:U_HDS_4:tau_charge": 0.10065565970206412,
                                "B12R1:I_HDS_1:tau_charge": 0.07962061957244776,
                                "B12R1:I_HDS_2:tau_charge": 0.07916735673467283,
                                "B12R1:I_HDS_3:tau_charge": 0.09039325816532769,
                                "B12R1:I_HDS_4:tau_charge": 0.09205080856156965,
                            },
                            "diff": {
                                "B12R1:U_HDS_1:tau_charge": 0.003,
                                "B12R1:U_HDS_2:tau_charge": 0.003,
                                "B12R1:U_HDS_3:tau_charge": 0.003,
                                "B12R1:U_HDS_4:tau_charge": 0.003,
                                "B12R1:I_HDS_1:tau_charge": 0.003,
                                "B12R1:I_HDS_2:tau_charge": 0.003,
                                "B12R1:I_HDS_3:tau_charge": 0.003,
                                "B12R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "B12R1:U_HDS_1:tau_charge": True,
                                "B12R1:U_HDS_2:tau_charge": True,
                                "B12R1:U_HDS_3:tau_charge": True,
                                "B12R1:U_HDS_4:tau_charge": True,
                                "B12R1:I_HDS_1:tau_charge": True,
                                "B12R1:I_HDS_2:tau_charge": True,
                                "B12R1:I_HDS_3:tau_charge": True,
                                "B12R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B12R1:R_HDS_1:first20mean": 11.468728214778679,
                                "B12R1:R_HDS_2:first20mean": 11.493475979639058,
                                "B12R1:R_HDS_3:first20mean": 12.926895066471738,
                                "B12R1:R_HDS_4:first20mean": 13.107733074955974,
                            },
                            "act": {
                                "B12R1:R_HDS_1:first20mean": 11.466402014022117,
                                "B12R1:R_HDS_2:first20mean": 11.488582121976917,
                                "B12R1:R_HDS_3:first20mean": 12.920033680234948,
                                "B12R1:R_HDS_4:first20mean": 13.100122041150229,
                            },
                            "diff": {
                                "B12R1:R_HDS_1:first20mean": 0.5,
                                "B12R1:R_HDS_2:first20mean": 0.5,
                                "B12R1:R_HDS_3:first20mean": 0.5,
                                "B12R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "B12R1:R_HDS_1:first20mean": True,
                                "B12R1:R_HDS_2:first20mean": True,
                                "B12R1:R_HDS_3:first20mean": True,
                                "B12R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B12R1_C_HDS_1:capacitance": 0.007784111340101572,
                                "B12R1_C_HDS_2:capacitance": 0.007689657398454249,
                                "B12R1_C_HDS_3:capacitance": 0.00767798539698038,
                                "B12R1_C_HDS_4:capacitance": 0.007718267848552965,
                            },
                            "act": {
                                "B12R1_C_HDS_1:capacitance": 0.007744572515203435,
                                "B12R1_C_HDS_2:capacitance": 0.007666161538791349,
                                "B12R1_C_HDS_3:capacitance": 0.007643592327524283,
                                "B12R1_C_HDS_4:capacitance": 0.007683566564180364,
                            },
                            "diff": {
                                "B12R1_C_HDS_1:capacitance": nan,
                                "B12R1_C_HDS_2:capacitance": nan,
                                "B12R1_C_HDS_3:capacitance": nan,
                                "B12R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "B12R1_C_HDS_1:capacitance": nan,
                                "B12R1_C_HDS_2:capacitance": nan,
                                "B12R1_C_HDS_3:capacitance": nan,
                                "B12R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="C12R1",
                    timestamp=1619674484023000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "C12R1:U_HDS_1:first": 780.0,
                                "C12R1:U_HDS_1:last20mean": 15.0,
                                "C12R1:U_HDS_2:first": 780.0,
                                "C12R1:U_HDS_2:last20mean": 15.0,
                                "C12R1:U_HDS_3:first": 780.0,
                                "C12R1:U_HDS_3:last20mean": 15.0,
                                "C12R1:U_HDS_4:first": 780.0,
                                "C12R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "C12R1:U_HDS_1:first": 980.0,
                                "C12R1:U_HDS_1:last20mean": 85.0,
                                "C12R1:U_HDS_2:first": 980.0,
                                "C12R1:U_HDS_2:last20mean": 85.0,
                                "C12R1:U_HDS_3:first": 980.0,
                                "C12R1:U_HDS_3:last20mean": 85.0,
                                "C12R1:U_HDS_4:first": 980.0,
                                "C12R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "C12R1:U_HDS_1:first": 901.20685,
                                "C12R1:U_HDS_1:last20mean": 27.600070949999996,
                                "C12R1:U_HDS_2:first": 914.6231,
                                "C12R1:U_HDS_2:last20mean": 27.6039043,
                                "C12R1:U_HDS_3:first": 897.5845,
                                "C12R1:U_HDS_3:last20mean": 26.07062,
                                "C12R1:U_HDS_4:first": 917.1722,
                                "C12R1:U_HDS_4:last20mean": 27.08258775,
                            },
                            "result": {
                                "C12R1:U_HDS_1:first": True,
                                "C12R1:U_HDS_1:last20mean": True,
                                "C12R1:U_HDS_2:first": True,
                                "C12R1:U_HDS_2:last20mean": True,
                                "C12R1:U_HDS_3:first": True,
                                "C12R1:U_HDS_3:last20mean": True,
                                "C12R1:U_HDS_4:first": True,
                                "C12R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C12R1:U_HDS_1:tau_charge": 0.0824365535543879,
                                "C12R1:U_HDS_2:tau_charge": 0.08194364412801787,
                                "C12R1:U_HDS_3:tau_charge": 0.08110250341935558,
                                "C12R1:U_HDS_4:tau_charge": 0.08116445745358697,
                                "C12R1:I_HDS_1:tau_charge": 0.07367817998182084,
                                "C12R1:I_HDS_2:tau_charge": 0.07332531397056578,
                                "C12R1:I_HDS_3:tau_charge": 0.07240274489586242,
                                "C12R1:I_HDS_4:tau_charge": 0.07235898096369711,
                            },
                            "act": {
                                "C12R1:U_HDS_1:tau_charge": 0.0818897014118594,
                                "C12R1:U_HDS_2:tau_charge": 0.08172916570704901,
                                "C12R1:U_HDS_3:tau_charge": 0.0807155322867486,
                                "C12R1:U_HDS_4:tau_charge": 0.08111964517945736,
                                "C12R1:I_HDS_1:tau_charge": 0.0734779497668076,
                                "C12R1:I_HDS_2:tau_charge": 0.07309404373197563,
                                "C12R1:I_HDS_3:tau_charge": 0.0721429254977924,
                                "C12R1:I_HDS_4:tau_charge": 0.07246080425162203,
                            },
                            "diff": {
                                "C12R1:U_HDS_1:tau_charge": 0.003,
                                "C12R1:U_HDS_2:tau_charge": 0.003,
                                "C12R1:U_HDS_3:tau_charge": 0.003,
                                "C12R1:U_HDS_4:tau_charge": 0.003,
                                "C12R1:I_HDS_1:tau_charge": 0.003,
                                "C12R1:I_HDS_2:tau_charge": 0.003,
                                "C12R1:I_HDS_3:tau_charge": 0.003,
                                "C12R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "C12R1:U_HDS_1:tau_charge": True,
                                "C12R1:U_HDS_2:tau_charge": True,
                                "C12R1:U_HDS_3:tau_charge": True,
                                "C12R1:U_HDS_4:tau_charge": True,
                                "C12R1:I_HDS_1:tau_charge": True,
                                "C12R1:I_HDS_2:tau_charge": True,
                                "C12R1:I_HDS_3:tau_charge": True,
                                "C12R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C12R1:R_HDS_1:first20mean": 10.984632646135207,
                                "C12R1:R_HDS_2:first20mean": 10.890075318720912,
                                "C12R1:R_HDS_3:first20mean": 10.702911358369656,
                                "C12R1:R_HDS_4:first20mean": 10.764137483337844,
                            },
                            "act": {
                                "C12R1:R_HDS_1:first20mean": 10.960068166065833,
                                "C12R1:R_HDS_2:first20mean": 10.867742274111508,
                                "C12R1:R_HDS_3:first20mean": 10.680446564123944,
                                "C12R1:R_HDS_4:first20mean": 10.754942039587203,
                            },
                            "diff": {
                                "C12R1:R_HDS_1:first20mean": 0.5,
                                "C12R1:R_HDS_2:first20mean": 0.5,
                                "C12R1:R_HDS_3:first20mean": 0.5,
                                "C12R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "C12R1:R_HDS_1:first20mean": True,
                                "C12R1:R_HDS_2:first20mean": True,
                                "C12R1:R_HDS_3:first20mean": True,
                                "C12R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "C12R1_C_HDS_1:capacitance": 0.007504716471642052,
                                "C12R1_C_HDS_2:capacitance": 0.007524616839623706,
                                "C12R1_C_HDS_3:capacitance": 0.007577611427747983,
                                "C12R1_C_HDS_4:capacitance": 0.007540265774124871,
                            },
                            "act": {
                                "C12R1_C_HDS_1:capacitance": 0.007471641614912883,
                                "C12R1_C_HDS_2:capacitance": 0.007520344487901538,
                                "C12R1_C_HDS_3:capacitance": 0.007557318114195279,
                                "C12R1_C_HDS_4:capacitance": 0.0075425460110216365,
                            },
                            "diff": {
                                "C12R1_C_HDS_1:capacitance": nan,
                                "C12R1_C_HDS_2:capacitance": nan,
                                "C12R1_C_HDS_3:capacitance": nan,
                                "C12R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "C12R1_C_HDS_1:capacitance": nan,
                                "C12R1_C_HDS_2:capacitance": nan,
                                "C12R1_C_HDS_3:capacitance": nan,
                                "C12R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="A13R1",
                    timestamp=1619674547661000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "A13R1:U_HDS_1:first": 780.0,
                                "A13R1:U_HDS_1:last20mean": 15.0,
                                "A13R1:U_HDS_2:first": 780.0,
                                "A13R1:U_HDS_2:last20mean": 15.0,
                                "A13R1:U_HDS_3:first": 780.0,
                                "A13R1:U_HDS_3:last20mean": 15.0,
                                "A13R1:U_HDS_4:first": 780.0,
                                "A13R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "A13R1:U_HDS_1:first": 980.0,
                                "A13R1:U_HDS_1:last20mean": 85.0,
                                "A13R1:U_HDS_2:first": 980.0,
                                "A13R1:U_HDS_2:last20mean": 85.0,
                                "A13R1:U_HDS_3:first": 980.0,
                                "A13R1:U_HDS_3:last20mean": 85.0,
                                "A13R1:U_HDS_4:first": 980.0,
                                "A13R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "A13R1:U_HDS_1:first": 923.8803,
                                "A13R1:U_HDS_1:last20mean": 32.00826255,
                                "A13R1:U_HDS_2:first": 935.59076,
                                "A13R1:U_HDS_2:last20mean": 31.965139,
                                "A13R1:U_HDS_3:first": 929.6301,
                                "A13R1:U_HDS_3:last20mean": 32.234422,
                                "A13R1:U_HDS_4:first": 938.82983,
                                "A13R1:U_HDS_4:last20mean": 32.9924395,
                            },
                            "result": {
                                "A13R1:U_HDS_1:first": True,
                                "A13R1:U_HDS_1:last20mean": True,
                                "A13R1:U_HDS_2:first": True,
                                "A13R1:U_HDS_2:last20mean": True,
                                "A13R1:U_HDS_3:first": True,
                                "A13R1:U_HDS_3:last20mean": True,
                                "A13R1:U_HDS_4:first": True,
                                "A13R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A13R1:U_HDS_1:tau_charge": 0.08638234082924343,
                                "A13R1:U_HDS_2:tau_charge": 0.08579576777728251,
                                "A13R1:U_HDS_3:tau_charge": 0.08669932015248245,
                                "A13R1:U_HDS_4:tau_charge": 0.08681837430321893,
                                "A13R1:I_HDS_1:tau_charge": 0.07768809188576592,
                                "A13R1:I_HDS_2:tau_charge": 0.07730370180610484,
                                "A13R1:I_HDS_3:tau_charge": 0.07776091381301332,
                                "A13R1:I_HDS_4:tau_charge": 0.07811956182650118,
                            },
                            "act": {
                                "A13R1:U_HDS_1:tau_charge": 0.08545546095534381,
                                "A13R1:U_HDS_2:tau_charge": 0.08507205795227755,
                                "A13R1:U_HDS_3:tau_charge": 0.08591062716587644,
                                "A13R1:U_HDS_4:tau_charge": 0.08627976259674859,
                                "A13R1:I_HDS_1:tau_charge": 0.07703708651090635,
                                "A13R1:I_HDS_2:tau_charge": 0.07669039831815895,
                                "A13R1:I_HDS_3:tau_charge": 0.07726802998879155,
                                "A13R1:I_HDS_4:tau_charge": 0.07771076220767222,
                            },
                            "diff": {
                                "A13R1:U_HDS_1:tau_charge": 0.003,
                                "A13R1:U_HDS_2:tau_charge": 0.003,
                                "A13R1:U_HDS_3:tau_charge": 0.003,
                                "A13R1:U_HDS_4:tau_charge": 0.003,
                                "A13R1:I_HDS_1:tau_charge": 0.003,
                                "A13R1:I_HDS_2:tau_charge": 0.003,
                                "A13R1:I_HDS_3:tau_charge": 0.003,
                                "A13R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "A13R1:U_HDS_1:tau_charge": True,
                                "A13R1:U_HDS_2:tau_charge": True,
                                "A13R1:U_HDS_3:tau_charge": True,
                                "A13R1:U_HDS_4:tau_charge": True,
                                "A13R1:I_HDS_1:tau_charge": True,
                                "A13R1:I_HDS_2:tau_charge": True,
                                "A13R1:I_HDS_3:tau_charge": True,
                                "A13R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A13R1:R_HDS_1:first20mean": 11.713841750286312,
                                "A13R1:R_HDS_2:first20mean": 11.713802370008764,
                                "A13R1:R_HDS_3:first20mean": 11.699579761094201,
                                "A13R1:R_HDS_4:first20mean": 11.765500162477819,
                            },
                            "act": {
                                "A13R1:R_HDS_1:first20mean": 11.644040912310079,
                                "A13R1:R_HDS_2:first20mean": 11.642404527280325,
                                "A13R1:R_HDS_3:first20mean": 11.628283505428977,
                                "A13R1:R_HDS_4:first20mean": 11.699265069675352,
                            },
                            "diff": {
                                "A13R1:R_HDS_1:first20mean": 0.5,
                                "A13R1:R_HDS_2:first20mean": 0.5,
                                "A13R1:R_HDS_3:first20mean": 0.5,
                                "A13R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "A13R1:R_HDS_1:first20mean": True,
                                "A13R1:R_HDS_2:first20mean": True,
                                "A13R1:R_HDS_3:first20mean": True,
                                "A13R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "A13R1_C_HDS_1:capacitance": 0.007374381750302548,
                                "A13R1_C_HDS_2:capacitance": 0.007324331166534639,
                                "A13R1_C_HDS_3:capacitance": 0.0074104644716208095,
                                "A13R1_C_HDS_4:capacitance": 0.007379063627069377,
                            },
                            "act": {
                                "A13R1_C_HDS_1:capacitance": 0.007338986662697167,
                                "A13R1_C_HDS_2:capacitance": 0.007307086586189121,
                                "A13R1_C_HDS_3:capacitance": 0.007388074699569094,
                                "A13R1_C_HDS_4:capacitance": 0.007374801928403765,
                            },
                            "diff": {
                                "A13R1_C_HDS_1:capacitance": nan,
                                "A13R1_C_HDS_2:capacitance": nan,
                                "A13R1_C_HDS_3:capacitance": nan,
                                "A13R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "A13R1_C_HDS_1:capacitance": nan,
                                "A13R1_C_HDS_2:capacitance": nan,
                                "A13R1_C_HDS_3:capacitance": nan,
                                "A13R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
                VoltageCurrentResult(
                    source="B13R1",
                    timestamp=1619674820213000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "B13R1:U_HDS_1:first": 780.0,
                                "B13R1:U_HDS_1:last20mean": 15.0,
                                "B13R1:U_HDS_2:first": 780.0,
                                "B13R1:U_HDS_2:last20mean": 15.0,
                                "B13R1:U_HDS_3:first": 780.0,
                                "B13R1:U_HDS_3:last20mean": 15.0,
                                "B13R1:U_HDS_4:first": 780.0,
                                "B13R1:U_HDS_4:last20mean": 15.0,
                            },
                            "max": {
                                "B13R1:U_HDS_1:first": 980.0,
                                "B13R1:U_HDS_1:last20mean": 85.0,
                                "B13R1:U_HDS_2:first": 980.0,
                                "B13R1:U_HDS_2:last20mean": 85.0,
                                "B13R1:U_HDS_3:first": 980.0,
                                "B13R1:U_HDS_3:last20mean": 85.0,
                                "B13R1:U_HDS_4:first": 980.0,
                                "B13R1:U_HDS_4:last20mean": 85.0,
                            },
                            "act": {
                                "B13R1:U_HDS_1:first": 894.21124,
                                "B13R1:U_HDS_1:last20mean": 25.5885941,
                                "B13R1:U_HDS_2:first": 901.32184,
                                "B13R1:U_HDS_2:last20mean": 26.18369945,
                                "B13R1:U_HDS_3:first": 894.53705,
                                "B13R1:U_HDS_3:last20mean": 23.984395100000004,
                                "B13R1:U_HDS_4:first": 901.9927,
                                "B13R1:U_HDS_4:last20mean": 25.90195875,
                            },
                            "result": {
                                "B13R1:U_HDS_1:first": True,
                                "B13R1:U_HDS_1:last20mean": True,
                                "B13R1:U_HDS_2:first": True,
                                "B13R1:U_HDS_2:last20mean": True,
                                "B13R1:U_HDS_3:first": True,
                                "B13R1:U_HDS_3:last20mean": True,
                                "B13R1:U_HDS_4:first": True,
                                "B13R1:U_HDS_4:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B13R1:U_HDS_1:tau_charge": 0.08207038855053132,
                                "B13R1:U_HDS_2:tau_charge": 0.08231251340073338,
                                "B13R1:U_HDS_3:tau_charge": 0.08015813144181586,
                                "B13R1:U_HDS_4:tau_charge": 0.08199045382945644,
                                "B13R1:I_HDS_1:tau_charge": 0.07310189144501764,
                                "B13R1:I_HDS_2:tau_charge": 0.07329948625013691,
                                "B13R1:I_HDS_3:tau_charge": 0.07143637886621004,
                                "B13R1:I_HDS_4:tau_charge": 0.07307111907043368,
                            },
                            "act": {
                                "B13R1:U_HDS_1:tau_charge": 0.08122972985477862,
                                "B13R1:U_HDS_2:tau_charge": 0.08165471946155213,
                                "B13R1:U_HDS_3:tau_charge": 0.07931427460331096,
                                "B13R1:U_HDS_4:tau_charge": 0.08129264622121198,
                                "B13R1:I_HDS_1:tau_charge": 0.07242072322961927,
                                "B13R1:I_HDS_2:tau_charge": 0.07269530247433444,
                                "B13R1:I_HDS_3:tau_charge": 0.07073436804743229,
                                "B13R1:I_HDS_4:tau_charge": 0.07236520770461645,
                            },
                            "diff": {
                                "B13R1:U_HDS_1:tau_charge": 0.003,
                                "B13R1:U_HDS_2:tau_charge": 0.003,
                                "B13R1:U_HDS_3:tau_charge": 0.003,
                                "B13R1:U_HDS_4:tau_charge": 0.003,
                                "B13R1:I_HDS_1:tau_charge": 0.003,
                                "B13R1:I_HDS_2:tau_charge": 0.003,
                                "B13R1:I_HDS_3:tau_charge": 0.003,
                                "B13R1:I_HDS_4:tau_charge": 0.003,
                            },
                            "result": {
                                "B13R1:U_HDS_1:tau_charge": True,
                                "B13R1:U_HDS_2:tau_charge": True,
                                "B13R1:U_HDS_3:tau_charge": True,
                                "B13R1:U_HDS_4:tau_charge": True,
                                "B13R1:I_HDS_1:tau_charge": True,
                                "B13R1:I_HDS_2:tau_charge": True,
                                "B13R1:I_HDS_3:tau_charge": True,
                                "B13R1:I_HDS_4:tau_charge": True,
                            },
                        }
                    ),
                    first_r_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B13R1:R_HDS_1:first20mean": 10.565547228961368,
                                "B13R1:R_HDS_2:first20mean": 10.500555980506302,
                                "B13R1:R_HDS_3:first20mean": 10.484521100879437,
                                "B13R1:R_HDS_4:first20mean": 10.458122039654103,
                            },
                            "act": {
                                "B13R1:R_HDS_1:first20mean": 10.49944035330616,
                                "B13R1:R_HDS_2:first20mean": 10.436964964323424,
                                "B13R1:R_HDS_3:first20mean": 10.41730832215263,
                                "B13R1:R_HDS_4:first20mean": 10.393558953288121,
                            },
                            "diff": {
                                "B13R1:R_HDS_1:first20mean": 0.5,
                                "B13R1:R_HDS_2:first20mean": 0.5,
                                "B13R1:R_HDS_3:first20mean": 0.5,
                                "B13R1:R_HDS_4:first20mean": 0.5,
                            },
                            "result": {
                                "B13R1:R_HDS_1:first20mean": True,
                                "B13R1:R_HDS_2:first20mean": True,
                                "B13R1:R_HDS_3:first20mean": True,
                                "B13R1:R_HDS_4:first20mean": True,
                            },
                        }
                    ),
                    capacitance_comp=pd.DataFrame(
                        {
                            "ref": {
                                "B13R1_C_HDS_1:capacitance": 0.007767736660678308,
                                "B13R1_C_HDS_2:capacitance": 0.007838871918167189,
                                "B13R1_C_HDS_3:capacitance": 0.007645378426973858,
                                "B13R1_C_HDS_4:capacitance": 0.007839883061086197,
                            },
                            "act": {
                                "B13R1_C_HDS_1:capacitance": 0.007736577105197828,
                                "B13R1_C_HDS_2:capacitance": 0.00782360770019557,
                                "B13R1_C_HDS_3:capacitance": 0.007613701366085849,
                                "B13R1_C_HDS_4:capacitance": 0.00782144466458182,
                            },
                            "diff": {
                                "B13R1_C_HDS_1:capacitance": nan,
                                "B13R1_C_HDS_2:capacitance": nan,
                                "B13R1_C_HDS_3:capacitance": nan,
                                "B13R1_C_HDS_4:capacitance": nan,
                            },
                            "result": {
                                "B13R1_C_HDS_1:capacitance": nan,
                                "B13R1_C_HDS_2:capacitance": nan,
                                "B13R1_C_HDS_3:capacitance": nan,
                                "B13R1_C_HDS_4:capacitance": nan,
                            },
                        }
                    ),
                ),
            ],
        )
    ],
)
def test_acceptance_voltage_current(
    circuit_name: str,
    discharge_level: int,
    t_start: str,
    t_end: str,
    analysis_result: bool,
    voltage_current_results: list[VoltageCurrentResult],
):
    # act
    analysis = QuenchHeaterVoltageCurrentAnalysis("_", circuit_name, discharge_level, t_start, t_end)
    analysis.query()
    analysis.analyze()

    # assert
    assert analysis.get_analysis_output() is analysis_result
    assert analysis.voltage_current_results == voltage_current_results
