import pandas as pd
import pytest
from numpy import nan
from nxcals.spark_session_builder import get_or_create  # type: ignore

from lhcsmqh.analyses import VoltageCurrentResult, VoltageResult, quench_heater_ccc

voltage_results = [
    VoltageResult(
        source="RQ10.L8",
        timestamp=1658401120803000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 0.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 0.0,
                },
                "max": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 20.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 20.0,
                },
                "act": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 851.67816,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 3.92563216,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 877.7468,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 5.5664236649999985,
                },
                "result": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": True,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": True,
                    "heat.RQ10.L8:U_HDS_1_B2:first": True,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.11530324330163416,
                    "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.11909642951168845,
                },
                "act": {
                    "heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.11534810010559181,
                    "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.11957557103666304,
                },
                "diff": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.005, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.005},
                "result": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": True, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": True},
            }
        ),
    ),
    VoltageResult(
        source="RQ10.L8",
        timestamp=1658401120804000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 0.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 0.0,
                },
                "max": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 20.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 20.0,
                },
                "act": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 851.67816,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 3.92563216,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 877.7468,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 5.5664236649999985,
                },
                "result": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": True,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": True,
                    "heat.RQ10.L8:U_HDS_1_B2:first": True,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.11530324330163416,
                    "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.11909642951168845,
                },
                "act": {
                    "heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.11534810010559181,
                    "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.11957557103666304,
                },
                "diff": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.005, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.005},
                "result": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": True, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": True},
            }
        ),
    ),
]


voltage_current_results = [
    VoltageCurrentResult(
        source="C30R7",
        timestamp=1658393366712000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "C30R7:U_HDS_1:first": 780.0,
                    "C30R7:U_HDS_1:last20mean": 15.0,
                    "C30R7:U_HDS_2:first": 780.0,
                    "C30R7:U_HDS_2:last20mean": 15.0,
                    "C30R7:U_HDS_3:first": 780.0,
                    "C30R7:U_HDS_3:last20mean": 15.0,
                    "C30R7:U_HDS_4:first": 780.0,
                    "C30R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "C30R7:U_HDS_1:first": 980.0,
                    "C30R7:U_HDS_1:last20mean": 85.0,
                    "C30R7:U_HDS_2:first": 980.0,
                    "C30R7:U_HDS_2:last20mean": 85.0,
                    "C30R7:U_HDS_3:first": 980.0,
                    "C30R7:U_HDS_3:last20mean": 85.0,
                    "C30R7:U_HDS_4:first": 980.0,
                    "C30R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "C30R7:U_HDS_1:first": 888.40393,
                    "C30R7:U_HDS_1:last20mean": 24.710788899999997,
                    "C30R7:U_HDS_2:first": 908.25995,
                    "C30R7:U_HDS_2:last20mean": 26.157825299999995,
                    "C30R7:U_HDS_3:first": 891.8538,
                    "C30R7:U_HDS_3:last20mean": 25.211980949999997,
                    "C30R7:U_HDS_4:first": 913.7031,
                    "C30R7:U_HDS_4:last20mean": 26.22011525,
                },
                "result": {
                    "C30R7:U_HDS_1:first": True,
                    "C30R7:U_HDS_1:last20mean": True,
                    "C30R7:U_HDS_2:first": True,
                    "C30R7:U_HDS_2:last20mean": True,
                    "C30R7:U_HDS_3:first": True,
                    "C30R7:U_HDS_3:last20mean": True,
                    "C30R7:U_HDS_4:first": True,
                    "C30R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7:U_HDS_1:tau_charge": 0.08033901758883537,
                    "C30R7:U_HDS_2:tau_charge": 0.08105490839419083,
                    "C30R7:U_HDS_3:tau_charge": 0.0807866169583049,
                    "C30R7:U_HDS_4:tau_charge": 0.08103577789139323,
                    "C30R7:I_HDS_1:tau_charge": 0.07148364105342361,
                    "C30R7:I_HDS_2:tau_charge": 0.07212314565704496,
                    "C30R7:I_HDS_3:tau_charge": 0.07196405134614832,
                    "C30R7:I_HDS_4:tau_charge": 0.07197771426328392,
                },
                "act": {
                    "C30R7:U_HDS_1:tau_charge": 0.08050071699978913,
                    "C30R7:U_HDS_2:tau_charge": 0.08110945147673916,
                    "C30R7:U_HDS_3:tau_charge": 0.08089825778794668,
                    "C30R7:U_HDS_4:tau_charge": 0.08123584533641406,
                    "C30R7:I_HDS_1:tau_charge": 0.07157237855873316,
                    "C30R7:I_HDS_2:tau_charge": 0.07209872605669784,
                    "C30R7:I_HDS_3:tau_charge": 0.07202715990518975,
                    "C30R7:I_HDS_4:tau_charge": 0.07208079329636141,
                },
                "diff": {
                    "C30R7:U_HDS_1:tau_charge": 0.003,
                    "C30R7:U_HDS_2:tau_charge": 0.003,
                    "C30R7:U_HDS_3:tau_charge": 0.003,
                    "C30R7:U_HDS_4:tau_charge": 0.003,
                    "C30R7:I_HDS_1:tau_charge": 0.003,
                    "C30R7:I_HDS_2:tau_charge": 0.003,
                    "C30R7:I_HDS_3:tau_charge": 0.003,
                    "C30R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "C30R7:U_HDS_1:tau_charge": True,
                    "C30R7:U_HDS_2:tau_charge": True,
                    "C30R7:U_HDS_3:tau_charge": True,
                    "C30R7:U_HDS_4:tau_charge": True,
                    "C30R7:I_HDS_1:tau_charge": True,
                    "C30R7:I_HDS_2:tau_charge": True,
                    "C30R7:I_HDS_3:tau_charge": True,
                    "C30R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7:R_HDS_1:first20mean": 10.42337647901183,
                    "C30R7:R_HDS_2:first20mean": 10.471266085058605,
                    "C30R7:R_HDS_3:first20mean": 10.431611690145044,
                    "C30R7:R_HDS_4:first20mean": 10.443780192323377,
                },
                "act": {
                    "C30R7:R_HDS_1:first20mean": 10.424187831781214,
                    "C30R7:R_HDS_2:first20mean": 10.47475044816252,
                    "C30R7:R_HDS_3:first20mean": 10.430886634418657,
                    "C30R7:R_HDS_4:first20mean": 10.444593882174425,
                },
                "diff": {
                    "C30R7:R_HDS_1:first20mean": 0.5,
                    "C30R7:R_HDS_2:first20mean": 0.5,
                    "C30R7:R_HDS_3:first20mean": 0.5,
                    "C30R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "C30R7:R_HDS_1:first20mean": True,
                    "C30R7:R_HDS_2:first20mean": True,
                    "C30R7:R_HDS_3:first20mean": True,
                    "C30R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7_C_HDS_1:capacitance": 0.007707580912058908,
                    "C30R7_C_HDS_2:capacitance": 0.007740697995426518,
                    "C30R7_C_HDS_3:capacitance": 0.00774440415900696,
                    "C30R7_C_HDS_4:capacitance": 0.007759238168470644,
                },
                "act": {
                    "C30R7_C_HDS_1:capacitance": 0.007722492946103573,
                    "C30R7_C_HDS_2:capacitance": 0.007743330199428987,
                    "C30R7_C_HDS_3:capacitance": 0.0077556453850248725,
                    "C30R7_C_HDS_4:capacitance": 0.007777788801827673,
                },
                "diff": {
                    "C30R7_C_HDS_1:capacitance": nan,
                    "C30R7_C_HDS_2:capacitance": nan,
                    "C30R7_C_HDS_3:capacitance": nan,
                    "C30R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "C30R7_C_HDS_1:capacitance": nan,
                    "C30R7_C_HDS_2:capacitance": nan,
                    "C30R7_C_HDS_3:capacitance": nan,
                    "C30R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
    VoltageCurrentResult(
        source="A30R7",
        timestamp=1658393366713000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "A30R7:U_HDS_1:first": 780.0,
                    "A30R7:U_HDS_1:last20mean": 15.0,
                    "A30R7:U_HDS_2:first": 780.0,
                    "A30R7:U_HDS_2:last20mean": 15.0,
                    "A30R7:U_HDS_3:first": 780.0,
                    "A30R7:U_HDS_3:last20mean": 15.0,
                    "A30R7:U_HDS_4:first": 780.0,
                    "A30R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "A30R7:U_HDS_1:first": 980.0,
                    "A30R7:U_HDS_1:last20mean": 85.0,
                    "A30R7:U_HDS_2:first": 980.0,
                    "A30R7:U_HDS_2:last20mean": 85.0,
                    "A30R7:U_HDS_3:first": 980.0,
                    "A30R7:U_HDS_3:last20mean": 85.0,
                    "A30R7:U_HDS_4:first": 980.0,
                    "A30R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "A30R7:U_HDS_1:first": 927.6177,
                    "A30R7:U_HDS_1:last20mean": 26.313070749999998,
                    "A30R7:U_HDS_2:first": 930.9526,
                    "A30R7:U_HDS_2:last20mean": 28.132887100000005,
                    "A30R7:U_HDS_3:first": 928.231,
                    "A30R7:U_HDS_3:last20mean": 27.981475350000004,
                    "A30R7:U_HDS_4:first": 932.3517,
                    "A30R7:U_HDS_4:last20mean": 29.874122749999998,
                },
                "result": {
                    "A30R7:U_HDS_1:first": True,
                    "A30R7:U_HDS_1:last20mean": True,
                    "A30R7:U_HDS_2:first": True,
                    "A30R7:U_HDS_2:last20mean": True,
                    "A30R7:U_HDS_3:first": True,
                    "A30R7:U_HDS_3:last20mean": True,
                    "A30R7:U_HDS_4:first": True,
                    "A30R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7:U_HDS_1:tau_charge": 0.08102304232536452,
                    "A30R7:U_HDS_2:tau_charge": 0.08275383676267394,
                    "A30R7:U_HDS_3:tau_charge": 0.08264698955196323,
                    "A30R7:U_HDS_4:tau_charge": 0.08470531368897147,
                    "A30R7:I_HDS_1:tau_charge": 0.072538835453838,
                    "A30R7:I_HDS_2:tau_charge": 0.07409424378618441,
                    "A30R7:I_HDS_3:tau_charge": 0.07390401599316436,
                    "A30R7:I_HDS_4:tau_charge": 0.0759519486828724,
                },
                "act": {
                    "A30R7:U_HDS_1:tau_charge": 0.08131366598126766,
                    "A30R7:U_HDS_2:tau_charge": 0.08300690152681633,
                    "A30R7:U_HDS_3:tau_charge": 0.08281679722705078,
                    "A30R7:U_HDS_4:tau_charge": 0.08490608758905734,
                    "A30R7:I_HDS_1:tau_charge": 0.07259245847153728,
                    "A30R7:I_HDS_2:tau_charge": 0.07406397514008967,
                    "A30R7:I_HDS_3:tau_charge": 0.07405422033941861,
                    "A30R7:I_HDS_4:tau_charge": 0.07597567879103548,
                },
                "diff": {
                    "A30R7:U_HDS_1:tau_charge": 0.003,
                    "A30R7:U_HDS_2:tau_charge": 0.003,
                    "A30R7:U_HDS_3:tau_charge": 0.003,
                    "A30R7:U_HDS_4:tau_charge": 0.003,
                    "A30R7:I_HDS_1:tau_charge": 0.003,
                    "A30R7:I_HDS_2:tau_charge": 0.003,
                    "A30R7:I_HDS_3:tau_charge": 0.003,
                    "A30R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "A30R7:U_HDS_1:tau_charge": True,
                    "A30R7:U_HDS_2:tau_charge": True,
                    "A30R7:U_HDS_3:tau_charge": True,
                    "A30R7:U_HDS_4:tau_charge": True,
                    "A30R7:I_HDS_1:tau_charge": True,
                    "A30R7:I_HDS_2:tau_charge": True,
                    "A30R7:I_HDS_3:tau_charge": True,
                    "A30R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7:R_HDS_1:first20mean": 11.070608799674966,
                    "A30R7:R_HDS_2:first20mean": 11.112810318492485,
                    "A30R7:R_HDS_3:first20mean": 11.189910462117012,
                    "A30R7:R_HDS_4:first20mean": 11.213335704996982,
                },
                "act": {
                    "A30R7:R_HDS_1:first20mean": 11.068470250022356,
                    "A30R7:R_HDS_2:first20mean": 11.109566125480564,
                    "A30R7:R_HDS_3:first20mean": 11.190346409949633,
                    "A30R7:R_HDS_4:first20mean": 11.20972472600589,
                },
                "diff": {
                    "A30R7:R_HDS_1:first20mean": 0.5,
                    "A30R7:R_HDS_2:first20mean": 0.5,
                    "A30R7:R_HDS_3:first20mean": 0.5,
                    "A30R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "A30R7:R_HDS_1:first20mean": True,
                    "A30R7:R_HDS_2:first20mean": True,
                    "A30R7:R_HDS_3:first20mean": True,
                    "A30R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7_C_HDS_1:capacitance": 0.007318752183506237,
                    "A30R7_C_HDS_2:capacitance": 0.007446706493762952,
                    "A30R7_C_HDS_3:capacitance": 0.007385849049620303,
                    "A30R7_C_HDS_4:capacitance": 0.00755398000357952,
                },
                "act": {
                    "A30R7_C_HDS_1:capacitance": 0.007346423141093361,
                    "A30R7_C_HDS_2:capacitance": 0.007471660062082373,
                    "A30R7_C_HDS_3:capacitance": 0.007400735794328598,
                    "A30R7_C_HDS_4:capacitance": 0.007574324050266846,
                },
                "diff": {
                    "A30R7_C_HDS_1:capacitance": nan,
                    "A30R7_C_HDS_2:capacitance": nan,
                    "A30R7_C_HDS_3:capacitance": nan,
                    "A30R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "A30R7_C_HDS_1:capacitance": nan,
                    "A30R7_C_HDS_2:capacitance": nan,
                    "A30R7_C_HDS_3:capacitance": nan,
                    "A30R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
    VoltageCurrentResult(
        source="B31R7",
        timestamp=1658393366713000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "B31R7:U_HDS_1:first": 780.0,
                    "B31R7:U_HDS_1:last20mean": 15.0,
                    "B31R7:U_HDS_2:first": 780.0,
                    "B31R7:U_HDS_2:last20mean": 15.0,
                    "B31R7:U_HDS_3:first": 780.0,
                    "B31R7:U_HDS_3:last20mean": 15.0,
                    "B31R7:U_HDS_4:first": 780.0,
                    "B31R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "B31R7:U_HDS_1:first": 980.0,
                    "B31R7:U_HDS_1:last20mean": 85.0,
                    "B31R7:U_HDS_2:first": 980.0,
                    "B31R7:U_HDS_2:last20mean": 85.0,
                    "B31R7:U_HDS_3:first": 980.0,
                    "B31R7:U_HDS_3:last20mean": 85.0,
                    "B31R7:U_HDS_4:first": 980.0,
                    "B31R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "B31R7:U_HDS_1:first": 891.7963,
                    "B31R7:U_HDS_1:last20mean": 28.6609121,
                    "B31R7:U_HDS_2:first": 893.2146,
                    "B31R7:U_HDS_2:last20mean": 27.141044100000006,
                    "B31R7:U_HDS_3:first": 895.2462,
                    "B31R7:U_HDS_3:last20mean": 27.23304145,
                    "B31R7:U_HDS_4:first": 885.7782,
                    "B31R7:U_HDS_4:last20mean": 28.53249945,
                },
                "result": {
                    "B31R7:U_HDS_1:first": True,
                    "B31R7:U_HDS_1:last20mean": True,
                    "B31R7:U_HDS_2:first": True,
                    "B31R7:U_HDS_2:last20mean": True,
                    "B31R7:U_HDS_3:first": True,
                    "B31R7:U_HDS_3:last20mean": True,
                    "B31R7:U_HDS_4:first": True,
                    "B31R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7:U_HDS_1:tau_charge": 0.08357063073852104,
                    "B31R7:U_HDS_2:tau_charge": 0.0825296953553959,
                    "B31R7:U_HDS_3:tau_charge": 0.08234042322611523,
                    "B31R7:U_HDS_4:tau_charge": 0.08374715078912603,
                    "B31R7:I_HDS_1:tau_charge": 0.074395459335003,
                    "B31R7:I_HDS_2:tau_charge": 0.07371828477139264,
                    "B31R7:I_HDS_3:tau_charge": 0.07334585837867007,
                    "B31R7:I_HDS_4:tau_charge": 0.07451485638328638,
                },
                "act": {
                    "B31R7:U_HDS_1:tau_charge": 0.08276153267033458,
                    "B31R7:U_HDS_2:tau_charge": 0.08208367193309625,
                    "B31R7:U_HDS_3:tau_charge": 0.08211321981827836,
                    "B31R7:U_HDS_4:tau_charge": 0.08321989534235641,
                    "B31R7:I_HDS_1:tau_charge": 0.07363959460495827,
                    "B31R7:I_HDS_2:tau_charge": 0.07327498081776128,
                    "B31R7:I_HDS_3:tau_charge": 0.07310809431945793,
                    "B31R7:I_HDS_4:tau_charge": 0.07400664830697516,
                },
                "diff": {
                    "B31R7:U_HDS_1:tau_charge": 0.003,
                    "B31R7:U_HDS_2:tau_charge": 0.003,
                    "B31R7:U_HDS_3:tau_charge": 0.003,
                    "B31R7:U_HDS_4:tau_charge": 0.003,
                    "B31R7:I_HDS_1:tau_charge": 0.003,
                    "B31R7:I_HDS_2:tau_charge": 0.003,
                    "B31R7:I_HDS_3:tau_charge": 0.003,
                    "B31R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "B31R7:U_HDS_1:tau_charge": True,
                    "B31R7:U_HDS_2:tau_charge": True,
                    "B31R7:U_HDS_3:tau_charge": True,
                    "B31R7:U_HDS_4:tau_charge": True,
                    "B31R7:I_HDS_1:tau_charge": True,
                    "B31R7:I_HDS_2:tau_charge": True,
                    "B31R7:I_HDS_3:tau_charge": True,
                    "B31R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7:R_HDS_1:first20mean": 10.486675564672144,
                    "B31R7:R_HDS_2:first20mean": 10.573151127956802,
                    "B31R7:R_HDS_3:first20mean": 10.574578669926979,
                    "B31R7:R_HDS_4:first20mean": 10.470002875365768,
                },
                "act": {
                    "B31R7:R_HDS_1:first20mean": 10.478264950093267,
                    "B31R7:R_HDS_2:first20mean": 10.568922546475717,
                    "B31R7:R_HDS_3:first20mean": 10.56600600837526,
                    "B31R7:R_HDS_4:first20mean": 10.463035265407186,
                },
                "diff": {
                    "B31R7:R_HDS_1:first20mean": 0.5,
                    "B31R7:R_HDS_2:first20mean": 0.5,
                    "B31R7:R_HDS_3:first20mean": 0.5,
                    "B31R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "B31R7:R_HDS_1:first20mean": True,
                    "B31R7:R_HDS_2:first20mean": True,
                    "B31R7:R_HDS_3:first20mean": True,
                    "B31R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7_C_HDS_1:capacitance": 0.007969220581216083,
                    "B31R7_C_HDS_2:capacitance": 0.007805591195719933,
                    "B31R7_C_HDS_3:capacitance": 0.007786638673395374,
                    "B31R7_C_HDS_4:capacitance": 0.00799877056253438,
                },
                "act": {
                    "B31R7_C_HDS_1:capacitance": 0.007898400456995308,
                    "B31R7_C_HDS_2:capacitance": 0.007766512771017291,
                    "B31R7_C_HDS_3:capacitance": 0.0077714530687556325,
                    "B31R7_C_HDS_4:capacitance": 0.007953704946163896,
                },
                "diff": {
                    "B31R7_C_HDS_1:capacitance": nan,
                    "B31R7_C_HDS_2:capacitance": nan,
                    "B31R7_C_HDS_3:capacitance": nan,
                    "B31R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "B31R7_C_HDS_1:capacitance": nan,
                    "B31R7_C_HDS_2:capacitance": nan,
                    "B31R7_C_HDS_3:capacitance": nan,
                    "B31R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
    VoltageCurrentResult(
        source="18R2",
        timestamp=1658393637926042968,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "18R2:U_HDS_1:first": 810.0,
                    "18R2:U_HDS_1:last20mean": 20.0,
                    "18R2:U_HDS_2:first": 810.0,
                    "18R2:U_HDS_2:last20mean": 20.0,
                },
                "max": {
                    "18R2:U_HDS_1:first": 1020.0,
                    "18R2:U_HDS_1:last20mean": 45.0,
                    "18R2:U_HDS_2:first": 1020.0,
                    "18R2:U_HDS_2:last20mean": 45.0,
                },
                "act": {
                    "18R2:U_HDS_1:first": 914.82446,
                    "18R2:U_HDS_1:last20mean": 27.260981400000002,
                    "18R2:U_HDS_2:first": 915.56726,
                    "18R2:U_HDS_2:last20mean": 27.474929049999997,
                },
                "result": {
                    "18R2:U_HDS_1:first": True,
                    "18R2:U_HDS_1:last20mean": True,
                    "18R2:U_HDS_2:first": True,
                    "18R2:U_HDS_2:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "18R2:U_HDS_1:tau_charge": 0.08228129379948082,
                    "18R2:U_HDS_2:tau_charge": 0.08260873643120471,
                    "18R2:I_HDS_1:tau_charge": 0.07297708077127855,
                    "18R2:I_HDS_2:tau_charge": 0.07318586569514528,
                },
                "act": {
                    "18R2:U_HDS_1:tau_charge": 0.08328160074983856,
                    "18R2:U_HDS_2:tau_charge": 0.083464569352813,
                    "18R2:I_HDS_1:tau_charge": 0.0737712434857393,
                    "18R2:I_HDS_2:tau_charge": 0.07395414775476841,
                },
                "diff": {
                    "18R2:U_HDS_1:tau_charge": 0.003,
                    "18R2:U_HDS_2:tau_charge": 0.003,
                    "18R2:I_HDS_1:tau_charge": 0.003,
                    "18R2:I_HDS_2:tau_charge": 0.003,
                },
                "result": {
                    "18R2:U_HDS_1:tau_charge": True,
                    "18R2:U_HDS_2:tau_charge": True,
                    "18R2:I_HDS_1:tau_charge": True,
                    "18R2:I_HDS_2:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {"18R2:R_HDS_1:first20mean": 10.970921018587077, "18R2:R_HDS_2:first20mean": 11.014593851410535},
                "act": {"18R2:R_HDS_1:first20mean": 10.988526972404937, "18R2:R_HDS_2:first20mean": 11.012254867114663},
                "diff": {"18R2:R_HDS_1:first20mean": 0.5, "18R2:R_HDS_2:first20mean": 0.5},
                "result": {"18R2:R_HDS_1:first20mean": True, "18R2:R_HDS_2:first20mean": True},
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "18R2_C_HDS_1:capacitance": 0.0074999440484603596,
                    "18R2_C_HDS_2:capacitance": 0.00749993486329283,
                },
                "act": {
                    "18R2_C_HDS_1:capacitance": 0.007578959487379922,
                    "18R2_C_HDS_2:capacitance": 0.0075792442474301065,
                },
                "diff": {"18R2_C_HDS_1:capacitance": nan, "18R2_C_HDS_2:capacitance": nan},
                "result": {"18R2_C_HDS_1:capacitance": nan, "18R2_C_HDS_2:capacitance": nan},
            }
        ),
    ),
    VoltageCurrentResult(
        source="29R7",
        timestamp=1658393297130110500,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "29R7:U_HDS_1:first": 810.0,
                    "29R7:U_HDS_1:last20mean": 20.0,
                    "29R7:U_HDS_2:first": 810.0,
                    "29R7:U_HDS_2:last20mean": 20.0,
                },
                "max": {
                    "29R7:U_HDS_1:first": 1020.0,
                    "29R7:U_HDS_1:last20mean": 45.0,
                    "29R7:U_HDS_2:first": 1020.0,
                    "29R7:U_HDS_2:last20mean": 45.0,
                },
                "act": {
                    "29R7:U_HDS_1:first": 912.30554,
                    "29R7:U_HDS_1:last20mean": 26.5093384,
                    "29R7:U_HDS_2:first": 908.5433,
                    "29R7:U_HDS_2:last20mean": 25.998285649999996,
                },
                "result": {
                    "29R7:U_HDS_1:first": True,
                    "29R7:U_HDS_1:last20mean": True,
                    "29R7:U_HDS_2:first": True,
                    "29R7:U_HDS_2:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "29R7:U_HDS_1:tau_charge": 0.0810654593155952,
                    "29R7:U_HDS_2:tau_charge": 0.08115745755466233,
                    "29R7:I_HDS_1:tau_charge": 0.07184079996529523,
                    "29R7:I_HDS_2:tau_charge": 0.07225868679788079,
                },
                "act": {
                    "29R7:U_HDS_1:tau_charge": 0.08223054220288487,
                    "29R7:U_HDS_2:tau_charge": 0.08215291321952317,
                    "29R7:I_HDS_1:tau_charge": 0.0727587355253246,
                    "29R7:I_HDS_2:tau_charge": 0.0728522663977371,
                },
                "diff": {
                    "29R7:U_HDS_1:tau_charge": 0.003,
                    "29R7:U_HDS_2:tau_charge": 0.003,
                    "29R7:I_HDS_1:tau_charge": 0.003,
                    "29R7:I_HDS_2:tau_charge": 0.003,
                },
                "result": {
                    "29R7:U_HDS_1:tau_charge": True,
                    "29R7:U_HDS_2:tau_charge": True,
                    "29R7:I_HDS_1:tau_charge": True,
                    "29R7:I_HDS_2:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {"29R7:R_HDS_1:first20mean": 10.81556434898576, "29R7:R_HDS_2:first20mean": 10.962061384576465},
                "act": {"29R7:R_HDS_1:first20mean": 10.818035463648254, "29R7:R_HDS_2:first20mean": 10.92488589559435},
                "diff": {"29R7:R_HDS_1:first20mean": 0.5, "29R7:R_HDS_2:first20mean": 0.5},
                "result": {"29R7:R_HDS_1:first20mean": True, "29R7:R_HDS_2:first20mean": True},
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "29R7_C_HDS_1:capacitance": 0.007495259304078496,
                    "29R7_C_HDS_2:capacitance": 0.007403485047880706,
                },
                "act": {
                    "29R7_C_HDS_1:capacitance": 0.007601245390552047,
                    "29R7_C_HDS_2:capacitance": 0.007519795996464618,
                },
                "diff": {"29R7_C_HDS_1:capacitance": nan, "29R7_C_HDS_2:capacitance": nan},
                "result": {"29R7_C_HDS_1:capacitance": nan, "29R7_C_HDS_2:capacitance": nan},
            }
        ),
    ),
]


@pytest.mark.parametrize(
    ("initial_charge_check", "voltage_results", "voltage_current_results"),
    [(False, voltage_results, voltage_current_results), (True, voltage_results, voltage_current_results)],
)
def test_acceptance_ccc(
    initial_charge_check: bool,
    voltage_results: list[VoltageResult],
    voltage_current_results: list[VoltageCurrentResult],
):
    # arrange
    spark = get_or_create()

    # act
    analysis = quench_heater_ccc.QHCCCAnalysis("_", "2022-07-21 07:00:00", "2022-07-21 23:59:59", initial_charge_check)
    analysis.set_spark(spark)
    analysis.query()
    analysis.analyze()

    # assert
    assert analysis.get_analysis_output() is True
    assert analysis.voltage_results == voltage_results
    assert analysis.voltage_current_results == voltage_current_results
