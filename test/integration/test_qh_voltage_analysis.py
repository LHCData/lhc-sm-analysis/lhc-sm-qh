import pandas as pd
import pytest

from lhcsmqh.analyses import QuenchHeaterVoltageAnalysis, VoltageResult


@pytest.mark.parametrize(
    ("circuit_name", "discharge_level", "t_start", "t_end", "analysis_result", "voltage_results"),
    [
        (
            "RQX.L1",
            900,
            "2021-04-28 12:00:00+01:00",
            "2021-04-29 12:00:00+01:00",
            False,
            [
                VoltageResult(
                    source="RQX.L1",
                    timestamp=1619614205229000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "heat.RQX.L1:U_HDS_1_Q1:first": 810.0,
                                "heat.RQX.L1:U_HDS_1_Q1:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_1_Q2:first": 810.0,
                                "heat.RQX.L1:U_HDS_1_Q2:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_1_Q3:first": 810.0,
                                "heat.RQX.L1:U_HDS_1_Q3:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_2_Q1:first": 810.0,
                                "heat.RQX.L1:U_HDS_2_Q1:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_2_Q2:first": 810.0,
                                "heat.RQX.L1:U_HDS_2_Q2:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_2_Q3:first": 810.0,
                                "heat.RQX.L1:U_HDS_2_Q3:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_3_Q2:first": 810.0,
                                "heat.RQX.L1:U_HDS_3_Q2:last20mean": 0.0,
                                "heat.RQX.L1:U_HDS_4_Q2:first": 810.0,
                                "heat.RQX.L1:U_HDS_4_Q2:last20mean": 0.0,
                            },
                            "max": {
                                "heat.RQX.L1:U_HDS_1_Q1:first": 1000.0,
                                "heat.RQX.L1:U_HDS_1_Q1:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_1_Q2:first": 1000.0,
                                "heat.RQX.L1:U_HDS_1_Q2:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_1_Q3:first": 1000.0,
                                "heat.RQX.L1:U_HDS_1_Q3:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_2_Q1:first": 1000.0,
                                "heat.RQX.L1:U_HDS_2_Q1:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_2_Q2:first": 1000.0,
                                "heat.RQX.L1:U_HDS_2_Q2:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_2_Q3:first": 1000.0,
                                "heat.RQX.L1:U_HDS_2_Q3:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_3_Q2:first": 1000.0,
                                "heat.RQX.L1:U_HDS_3_Q2:last20mean": 20.0,
                                "heat.RQX.L1:U_HDS_4_Q2:first": 1000.0,
                                "heat.RQX.L1:U_HDS_4_Q2:last20mean": 20.0,
                            },
                            "act": {
                                "heat.RQX.L1:U_HDS_1_Q1:first": 885.72076,
                                "heat.RQX.L1:U_HDS_1_Q1:last20mean": 4.201653090000001,
                                "heat.RQX.L1:U_HDS_1_Q2:first": 862.4123,
                                "heat.RQX.L1:U_HDS_1_Q2:last20mean": 3.20491051,
                                "heat.RQX.L1:U_HDS_1_Q3:first": 851.06476,
                                "heat.RQX.L1:U_HDS_1_Q3:last20mean": 4.585015500000001,
                                "heat.RQX.L1:U_HDS_2_Q1:first": 868.8528,
                                "heat.RQX.L1:U_HDS_2_Q1:last20mean": 4.201653090000001,
                                "heat.RQX.L1:U_HDS_2_Q2:first": 850.75806,
                                "heat.RQX.L1:U_HDS_2_Q2:last20mean": 2.6988719999999993,
                                "heat.RQX.L1:U_HDS_2_Q3:first": 887.2542,
                                "heat.RQX.L1:U_HDS_2_Q3:last20mean": 4.17098409,
                                "heat.RQX.L1:U_HDS_3_Q2:first": 828.9831,
                                "heat.RQX.L1:U_HDS_3_Q2:last20mean": 3.2969175299999995,
                                "heat.RQX.L1:U_HDS_4_Q2:first": 863.94574,
                                "heat.RQX.L1:U_HDS_4_Q2:last20mean": 3.2049104999999996,
                            },
                            "result": {
                                "heat.RQX.L1:U_HDS_1_Q1:first": True,
                                "heat.RQX.L1:U_HDS_1_Q1:last20mean": True,
                                "heat.RQX.L1:U_HDS_1_Q2:first": True,
                                "heat.RQX.L1:U_HDS_1_Q2:last20mean": True,
                                "heat.RQX.L1:U_HDS_1_Q3:first": True,
                                "heat.RQX.L1:U_HDS_1_Q3:last20mean": True,
                                "heat.RQX.L1:U_HDS_2_Q1:first": True,
                                "heat.RQX.L1:U_HDS_2_Q1:last20mean": True,
                                "heat.RQX.L1:U_HDS_2_Q2:first": True,
                                "heat.RQX.L1:U_HDS_2_Q2:last20mean": True,
                                "heat.RQX.L1:U_HDS_2_Q3:first": True,
                                "heat.RQX.L1:U_HDS_2_Q3:last20mean": True,
                                "heat.RQX.L1:U_HDS_3_Q2:first": True,
                                "heat.RQX.L1:U_HDS_3_Q2:last20mean": True,
                                "heat.RQX.L1:U_HDS_4_Q2:first": True,
                                "heat.RQX.L1:U_HDS_4_Q2:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "heat.RQX.L1:U_HDS_1_Q1:tau_charge": 0.09554789908868778,
                                "heat.RQX.L1:U_HDS_2_Q1:tau_charge": 0.09117852473845597,
                                "heat.RQX.L1:U_HDS_1_Q2:tau_charge": 0.15147123031618348,
                                "heat.RQX.L1:U_HDS_2_Q2:tau_charge": 0.15063191076201596,
                                "heat.RQX.L1:U_HDS_3_Q2:tau_charge": 0.08969346960764893,
                                "heat.RQX.L1:U_HDS_4_Q2:tau_charge": 0.09175190244463913,
                                "heat.RQX.L1:U_HDS_1_Q3:tau_charge": 0.14454873627496315,
                                "heat.RQX.L1:U_HDS_2_Q3:tau_charge": 0.13946394408097884,
                            },
                            "act": {
                                "heat.RQX.L1:U_HDS_1_Q1:tau_charge": 0.14998677986152412,
                                "heat.RQX.L1:U_HDS_2_Q1:tau_charge": 0.14212340240060667,
                                "heat.RQX.L1:U_HDS_1_Q2:tau_charge": 0.09714846563582001,
                                "heat.RQX.L1:U_HDS_2_Q2:tau_charge": 0.09741359617856106,
                                "heat.RQX.L1:U_HDS_3_Q2:tau_charge": 0.08906683833347742,
                                "heat.RQX.L1:U_HDS_4_Q2:tau_charge": 0.09248307882303158,
                                "heat.RQX.L1:U_HDS_1_Q3:tau_charge": 0.1437388647860089,
                                "heat.RQX.L1:U_HDS_2_Q3:tau_charge": 0.1395364573449728,
                            },
                            "diff": {
                                "heat.RQX.L1:U_HDS_1_Q1:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_2_Q1:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_1_Q2:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_2_Q2:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_3_Q2:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_4_Q2:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_1_Q3:tau_charge": 0.005,
                                "heat.RQX.L1:U_HDS_2_Q3:tau_charge": 0.005,
                            },
                            "result": {
                                "heat.RQX.L1:U_HDS_1_Q1:tau_charge": False,
                                "heat.RQX.L1:U_HDS_2_Q1:tau_charge": False,
                                "heat.RQX.L1:U_HDS_1_Q2:tau_charge": False,
                                "heat.RQX.L1:U_HDS_2_Q2:tau_charge": False,
                                "heat.RQX.L1:U_HDS_3_Q2:tau_charge": True,
                                "heat.RQX.L1:U_HDS_4_Q2:tau_charge": True,
                                "heat.RQX.L1:U_HDS_1_Q3:tau_charge": True,
                                "heat.RQX.L1:U_HDS_2_Q3:tau_charge": True,
                            },
                        }
                    ),
                )
            ],
        ),
        (
            "RQ10.L1",
            900,
            "2021-04-28 12:00:00+01:00",
            "2021-04-29 12:00:00+01:00",
            True,
            [
                VoltageResult(
                    source="RQ10.L1",
                    timestamp=1619615456267000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 810.0,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 0.0,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 810.0,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 0.0,
                            },
                            "max": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 1000.0,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 20.0,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 1000.0,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 20.0,
                            },
                            "act": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 883.5739,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 3.46559709,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 922.83026,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 2.913555,
                            },
                            "result": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": True,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": True,
                                "heat.RQ10.L1:U_HDS_1_B2:first": True,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.10359600967160373,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.1031478718670971,
                            },
                            "act": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.10230494718497411,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.10270333089638121,
                            },
                            "diff": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.005,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.005,
                            },
                            "result": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": True,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": True,
                            },
                        }
                    ),
                ),
                VoltageResult(
                    source="RQ10.L1",
                    timestamp=1619615456268000000,
                    first_last_u_comp=pd.DataFrame(
                        {
                            "min": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 810.0,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 0.0,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 810.0,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 0.0,
                            },
                            "max": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 1000.0,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 20.0,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 1000.0,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 20.0,
                            },
                            "act": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": 883.5739,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": 3.46559709,
                                "heat.RQ10.L1:U_HDS_1_B2:first": 922.83026,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": 2.913555,
                            },
                            "result": {
                                "heat.RQ10.L1:U_HDS_1_B1:first": True,
                                "heat.RQ10.L1:U_HDS_1_B1:last20mean": True,
                                "heat.RQ10.L1:U_HDS_1_B2:first": True,
                                "heat.RQ10.L1:U_HDS_1_B2:last20mean": True,
                            },
                        }
                    ),
                    tau_u_comp=pd.DataFrame(
                        {
                            "ref": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.10359600967160373,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.1031478718670971,
                            },
                            "act": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.10230494718497411,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.10270333089638121,
                            },
                            "diff": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": 0.005,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": 0.005,
                            },
                            "result": {
                                "heat.RQ10.L1:U_HDS_1_B1:tau_charge": True,
                                "heat.RQ10.L1:U_HDS_1_B2:tau_charge": True,
                            },
                        }
                    ),
                ),
            ],
        ),
    ],
)
def test_acceptance_voltage(
    circuit_name: str,
    discharge_level: int,
    t_start: str,
    t_end: str,
    analysis_result: bool,
    voltage_results: list[VoltageResult],
):
    # act
    analysis = QuenchHeaterVoltageAnalysis("_", circuit_name, discharge_level, t_start, t_end)
    analysis.query()
    analysis.analyze()

    # assert
    assert analysis.get_analysis_output() is analysis_result
    assert analysis.voltage_results == voltage_results
