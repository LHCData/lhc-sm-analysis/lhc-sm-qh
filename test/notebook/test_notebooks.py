import pathlib

import papermill  # type: ignore
import pytest


@pytest.mark.parametrize(
    ("notebook_name", "notebook_parameters"),
    [
        (
            "HWC_QHD_PM_LIST_CCC.ipynb",
            {"start_time": "2021-10-18 07:00:00", "stop_time": "2021-10-20 23:01:00", "initial_charge_check": True},
        ),
        (
            "HWC_voltage_current_QHDA.ipynb",
            {
                "circuit_type": "RB",
                "circuit_name": "RB.A45",
                "discharge_level": 300,
                "start_time": "2023-03-09 14:00:00+01:00",
                "stop_time": "2023-03-09 16:10:00+01:00",
                "is_automatic": True,
                "signature": "UNKNOWN",
            },
        ),
        (
            "HWC_voltage_QHDA.ipynb",
            {
                "circuit_type": "IT",
                "circuit_name": "RQX.L1",
                "discharge_level": 900,
                "start_time": "2021-04-07 00:00:00+01:00",
                "stop_time": "2021-05-11 00:00:00+01:00",
                "is_automatic": True,
                "signature": "UNKNOWN",
            },
        ),
        (
            "LHC_QH_States_by_Sector.ipynb",
            {"sector_in": "S12", "t_start": "2023-08-24 15:51:58", "t_end": "2023-08-24 15:56:58"},
        ),
        (
            "LHC_QH_States.ipynb",
            {
                "start_time": "2022-10-17 08:00:00",
                "stop_time": "2022-10-17 08:05:00",
                "t_start": "",
                "t_end": "",
                "circuits": ["RB.A12"],
            },
        ),
    ],
)
def test_notebook(notebook_name: str, notebook_parameters: dict):
    notebook_path = pathlib.Path(__file__).parent.parent.parent / "src" / "lhcsmqh" / "notebooks" / notebook_name
    report_path = pathlib.Path(__file__).parent.parent.parent / "reports" / f"report_{notebook_name}"

    papermill.execute_notebook(notebook_path, report_path, notebook_parameters)
