from unittest.mock import MagicMock

import pandas as pd
import pytest
from lhcsmapi.api.analysis.output import output_types
from lhcsmapi.metadata.signal_metadata import GenericCircuitType
from numpy import nan

from lhcsmqh import analyses
from lhcsmqh.analyses import commons
from lhcsmqh.output import quench_heater_ccc_output, quench_heater_voltage_current_output, quench_heater_voltage_output

voltage_events = [
    commons.VoltageEvent(
        source="RQ10.L8",
        circuit_type=GenericCircuitType.IPQ,
        circuit_name="RQD10.L8",
        signals=commons.VoltageSignals(timestamp=1658401120803000000, u_hds=MagicMock()),
        reference_signals=commons.VoltageSignals(timestamp=1658401120803000000, u_hds=MagicMock()),
    )
]

voltage_results = [
    commons.VoltageResult(
        source="RQ10.L8",
        timestamp=1658401120803000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 0.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 810.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 0.0,
                },
                "max": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 20.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 1000.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 20.0,
                },
                "act": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": 852.0,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": 4.0,
                    "heat.RQ10.L8:U_HDS_1_B2:first": 878.0,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": 6.0,
                },
                "result": {
                    "heat.RQ10.L8:U_HDS_1_B1:first": True,
                    "heat.RQ10.L8:U_HDS_1_B1:last20mean": True,
                    "heat.RQ10.L8:U_HDS_1_B2:first": True,
                    "heat.RQ10.L8:U_HDS_1_B2:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.115, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.119},
                "act": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.115, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.12},
                "diff": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": 0.005, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": 0.005},
                "result": {"heat.RQ10.L8:U_HDS_1_B1:tau_charge": True, "heat.RQ10.L8:U_HDS_1_B2:tau_charge": True},
            }
        ),
    )
]


voltage_current_events = [
    commons.VoltageCurrentEvent(
        source="C30R7",
        circuit_type=GenericCircuitType.RB,
        circuit_name="RB.A78",
        signals=MagicMock(timestamp=1658393366712000000),
        reference_signals=MagicMock(timestamp=1658393366712000000),
    ),
    commons.VoltageCurrentEvent(
        source="A30R7",
        circuit_type=GenericCircuitType.RB,
        circuit_name="RB.A78",
        signals=MagicMock(timestamp=1658393366713000000),
        reference_signals=MagicMock(timestamp=1658393366713000000),
    ),
    commons.VoltageCurrentEvent(
        source="B31R7",
        circuit_type=GenericCircuitType.RB,
        circuit_name="RQD10.L8",
        signals=MagicMock(timestamp=1658393366713000000),
        reference_signals=MagicMock(timestamp=1658393366713000000),
    ),
]


voltage_current_results = [
    commons.VoltageCurrentResult(
        source="C30R7",
        timestamp=1658393366712000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "C30R7:U_HDS_1:first": 780.0,
                    "C30R7:U_HDS_1:last20mean": 15.0,
                    "C30R7:U_HDS_2:first": 780.0,
                    "C30R7:U_HDS_2:last20mean": 15.0,
                    "C30R7:U_HDS_3:first": 780.0,
                    "C30R7:U_HDS_3:last20mean": 15.0,
                    "C30R7:U_HDS_4:first": 780.0,
                    "C30R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "C30R7:U_HDS_1:first": 980.0,
                    "C30R7:U_HDS_1:last20mean": 85.0,
                    "C30R7:U_HDS_2:first": 980.0,
                    "C30R7:U_HDS_2:last20mean": 85.0,
                    "C30R7:U_HDS_3:first": 980.0,
                    "C30R7:U_HDS_3:last20mean": 85.0,
                    "C30R7:U_HDS_4:first": 980.0,
                    "C30R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "C30R7:U_HDS_1:first": 888.0,
                    "C30R7:U_HDS_1:last20mean": 25.0,
                    "C30R7:U_HDS_2:first": 908.0,
                    "C30R7:U_HDS_2:last20mean": 26.0,
                    "C30R7:U_HDS_3:first": 892.0,
                    "C30R7:U_HDS_3:last20mean": 25.0,
                    "C30R7:U_HDS_4:first": 914.0,
                    "C30R7:U_HDS_4:last20mean": 26.0,
                },
                "result": {
                    "C30R7:U_HDS_1:first": True,
                    "C30R7:U_HDS_1:last20mean": True,
                    "C30R7:U_HDS_2:first": True,
                    "C30R7:U_HDS_2:last20mean": True,
                    "C30R7:U_HDS_3:first": True,
                    "C30R7:U_HDS_3:last20mean": True,
                    "C30R7:U_HDS_4:first": True,
                    "C30R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7:U_HDS_1:tau_charge": 0.08,
                    "C30R7:U_HDS_2:tau_charge": 0.081,
                    "C30R7:U_HDS_3:tau_charge": 0.081,
                    "C30R7:U_HDS_4:tau_charge": 0.081,
                    "C30R7:I_HDS_1:tau_charge": 0.071,
                    "C30R7:I_HDS_2:tau_charge": 0.072,
                    "C30R7:I_HDS_3:tau_charge": 0.072,
                    "C30R7:I_HDS_4:tau_charge": 0.072,
                },
                "act": {
                    "C30R7:U_HDS_1:tau_charge": 0.081,
                    "C30R7:U_HDS_2:tau_charge": 0.081,
                    "C30R7:U_HDS_3:tau_charge": 0.081,
                    "C30R7:U_HDS_4:tau_charge": 0.081,
                    "C30R7:I_HDS_1:tau_charge": 0.072,
                    "C30R7:I_HDS_2:tau_charge": 0.072,
                    "C30R7:I_HDS_3:tau_charge": 0.072,
                    "C30R7:I_HDS_4:tau_charge": 0.072,
                },
                "diff": {
                    "C30R7:U_HDS_1:tau_charge": 0.003,
                    "C30R7:U_HDS_2:tau_charge": 0.003,
                    "C30R7:U_HDS_3:tau_charge": 0.003,
                    "C30R7:U_HDS_4:tau_charge": 0.003,
                    "C30R7:I_HDS_1:tau_charge": 0.003,
                    "C30R7:I_HDS_2:tau_charge": 0.003,
                    "C30R7:I_HDS_3:tau_charge": 0.003,
                    "C30R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "C30R7:U_HDS_1:tau_charge": True,
                    "C30R7:U_HDS_2:tau_charge": True,
                    "C30R7:U_HDS_3:tau_charge": True,
                    "C30R7:U_HDS_4:tau_charge": True,
                    "C30R7:I_HDS_1:tau_charge": True,
                    "C30R7:I_HDS_2:tau_charge": True,
                    "C30R7:I_HDS_3:tau_charge": True,
                    "C30R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7:R_HDS_1:first20mean": 10.42,
                    "C30R7:R_HDS_2:first20mean": 10.47,
                    "C30R7:R_HDS_3:first20mean": 10.43,
                    "C30R7:R_HDS_4:first20mean": 10.44,
                },
                "act": {
                    "C30R7:R_HDS_1:first20mean": 10.42,
                    "C30R7:R_HDS_2:first20mean": 10.47,
                    "C30R7:R_HDS_3:first20mean": 10.43,
                    "C30R7:R_HDS_4:first20mean": 10.44,
                },
                "diff": {
                    "C30R7:R_HDS_1:first20mean": 0.5,
                    "C30R7:R_HDS_2:first20mean": 0.5,
                    "C30R7:R_HDS_3:first20mean": 0.5,
                    "C30R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "C30R7:R_HDS_1:first20mean": True,
                    "C30R7:R_HDS_2:first20mean": True,
                    "C30R7:R_HDS_3:first20mean": True,
                    "C30R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "C30R7_C_HDS_1:capacitance": 0.007677543186180423,
                    "C30R7_C_HDS_2:capacitance": 0.007736389684813753,
                    "C30R7_C_HDS_3:capacitance": 0.007766059443911793,
                    "C30R7_C_HDS_4:capacitance": 0.007758620689655173,
                },
                "act": {
                    "C30R7_C_HDS_1:capacitance": 0.0077735124760076775,
                    "C30R7_C_HDS_2:capacitance": 0.007736389684813753,
                    "C30R7_C_HDS_3:capacitance": 0.007766059443911793,
                    "C30R7_C_HDS_4:capacitance": 0.007758620689655173,
                },
                "diff": {
                    "C30R7_C_HDS_1:capacitance": nan,
                    "C30R7_C_HDS_2:capacitance": nan,
                    "C30R7_C_HDS_3:capacitance": nan,
                    "C30R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "C30R7_C_HDS_1:capacitance": nan,
                    "C30R7_C_HDS_2:capacitance": nan,
                    "C30R7_C_HDS_3:capacitance": nan,
                    "C30R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
    commons.VoltageCurrentResult(
        source="A30R7",
        timestamp=1658393366713000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "A30R7:U_HDS_1:first": 780.0,
                    "A30R7:U_HDS_1:last20mean": 15.0,
                    "A30R7:U_HDS_2:first": 780.0,
                    "A30R7:U_HDS_2:last20mean": 15.0,
                    "A30R7:U_HDS_3:first": 780.0,
                    "A30R7:U_HDS_3:last20mean": 15.0,
                    "A30R7:U_HDS_4:first": 780.0,
                    "A30R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "A30R7:U_HDS_1:first": 980.0,
                    "A30R7:U_HDS_1:last20mean": 85.0,
                    "A30R7:U_HDS_2:first": 980.0,
                    "A30R7:U_HDS_2:last20mean": 85.0,
                    "A30R7:U_HDS_3:first": 980.0,
                    "A30R7:U_HDS_3:last20mean": 85.0,
                    "A30R7:U_HDS_4:first": 980.0,
                    "A30R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "A30R7:U_HDS_1:first": 928.0,
                    "A30R7:U_HDS_1:last20mean": 26.0,
                    "A30R7:U_HDS_2:first": 931.0,
                    "A30R7:U_HDS_2:last20mean": 28.0,
                    "A30R7:U_HDS_3:first": 928.0,
                    "A30R7:U_HDS_3:last20mean": 28.0,
                    "A30R7:U_HDS_4:first": 932.0,
                    "A30R7:U_HDS_4:last20mean": 30.0,
                },
                "result": {
                    "A30R7:U_HDS_1:first": True,
                    "A30R7:U_HDS_1:last20mean": True,
                    "A30R7:U_HDS_2:first": True,
                    "A30R7:U_HDS_2:last20mean": True,
                    "A30R7:U_HDS_3:first": True,
                    "A30R7:U_HDS_3:last20mean": True,
                    "A30R7:U_HDS_4:first": True,
                    "A30R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7:U_HDS_1:tau_charge": 0.081,
                    "A30R7:U_HDS_2:tau_charge": 0.083,
                    "A30R7:U_HDS_3:tau_charge": 0.083,
                    "A30R7:U_HDS_4:tau_charge": 0.085,
                    "A30R7:I_HDS_1:tau_charge": 0.073,
                    "A30R7:I_HDS_2:tau_charge": 0.074,
                    "A30R7:I_HDS_3:tau_charge": 0.074,
                    "A30R7:I_HDS_4:tau_charge": 0.076,
                },
                "act": {
                    "A30R7:U_HDS_1:tau_charge": 0.081,
                    "A30R7:U_HDS_2:tau_charge": 0.083,
                    "A30R7:U_HDS_3:tau_charge": 0.083,
                    "A30R7:U_HDS_4:tau_charge": 0.085,
                    "A30R7:I_HDS_1:tau_charge": 0.073,
                    "A30R7:I_HDS_2:tau_charge": 0.074,
                    "A30R7:I_HDS_3:tau_charge": 0.074,
                    "A30R7:I_HDS_4:tau_charge": 0.076,
                },
                "diff": {
                    "A30R7:U_HDS_1:tau_charge": 0.003,
                    "A30R7:U_HDS_2:tau_charge": 0.003,
                    "A30R7:U_HDS_3:tau_charge": 0.003,
                    "A30R7:U_HDS_4:tau_charge": 0.003,
                    "A30R7:I_HDS_1:tau_charge": 0.003,
                    "A30R7:I_HDS_2:tau_charge": 0.003,
                    "A30R7:I_HDS_3:tau_charge": 0.003,
                    "A30R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "A30R7:U_HDS_1:tau_charge": True,
                    "A30R7:U_HDS_2:tau_charge": True,
                    "A30R7:U_HDS_3:tau_charge": True,
                    "A30R7:U_HDS_4:tau_charge": True,
                    "A30R7:I_HDS_1:tau_charge": True,
                    "A30R7:I_HDS_2:tau_charge": True,
                    "A30R7:I_HDS_3:tau_charge": True,
                    "A30R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7:R_HDS_1:first20mean": 11.07,
                    "A30R7:R_HDS_2:first20mean": 11.11,
                    "A30R7:R_HDS_3:first20mean": 11.19,
                    "A30R7:R_HDS_4:first20mean": 11.21,
                },
                "act": {
                    "A30R7:R_HDS_1:first20mean": 11.07,
                    "A30R7:R_HDS_2:first20mean": 11.11,
                    "A30R7:R_HDS_3:first20mean": 11.19,
                    "A30R7:R_HDS_4:first20mean": 11.21,
                },
                "diff": {
                    "A30R7:R_HDS_1:first20mean": 0.5,
                    "A30R7:R_HDS_2:first20mean": 0.5,
                    "A30R7:R_HDS_3:first20mean": 0.5,
                    "A30R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "A30R7:R_HDS_1:first20mean": True,
                    "A30R7:R_HDS_2:first20mean": True,
                    "A30R7:R_HDS_3:first20mean": True,
                    "A30R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "A30R7_C_HDS_1:capacitance": 0.007317073170731708,
                    "A30R7_C_HDS_2:capacitance": 0.007470747074707472,
                    "A30R7_C_HDS_3:capacitance": 0.007417336907953531,
                    "A30R7_C_HDS_4:capacitance": 0.007582515611061552,
                },
                "act": {
                    "A30R7_C_HDS_1:capacitance": 0.007317073170731708,
                    "A30R7_C_HDS_2:capacitance": 0.007470747074707472,
                    "A30R7_C_HDS_3:capacitance": 0.007417336907953531,
                    "A30R7_C_HDS_4:capacitance": 0.007582515611061552,
                },
                "diff": {
                    "A30R7_C_HDS_1:capacitance": nan,
                    "A30R7_C_HDS_2:capacitance": nan,
                    "A30R7_C_HDS_3:capacitance": nan,
                    "A30R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "A30R7_C_HDS_1:capacitance": nan,
                    "A30R7_C_HDS_2:capacitance": nan,
                    "A30R7_C_HDS_3:capacitance": nan,
                    "A30R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
    commons.VoltageCurrentResult(
        source="B31R7",
        timestamp=1658393366713000000,
        first_last_u_comp=pd.DataFrame(
            {
                "min": {
                    "B31R7:U_HDS_1:first": 780.0,
                    "B31R7:U_HDS_1:last20mean": 15.0,
                    "B31R7:U_HDS_2:first": 780.0,
                    "B31R7:U_HDS_2:last20mean": 15.0,
                    "B31R7:U_HDS_3:first": 780.0,
                    "B31R7:U_HDS_3:last20mean": 15.0,
                    "B31R7:U_HDS_4:first": 780.0,
                    "B31R7:U_HDS_4:last20mean": 15.0,
                },
                "max": {
                    "B31R7:U_HDS_1:first": 980.0,
                    "B31R7:U_HDS_1:last20mean": 85.0,
                    "B31R7:U_HDS_2:first": 980.0,
                    "B31R7:U_HDS_2:last20mean": 85.0,
                    "B31R7:U_HDS_3:first": 980.0,
                    "B31R7:U_HDS_3:last20mean": 85.0,
                    "B31R7:U_HDS_4:first": 980.0,
                    "B31R7:U_HDS_4:last20mean": 85.0,
                },
                "act": {
                    "B31R7:U_HDS_1:first": 892.0,
                    "B31R7:U_HDS_1:last20mean": 29.0,
                    "B31R7:U_HDS_2:first": 893.0,
                    "B31R7:U_HDS_2:last20mean": 27.0,
                    "B31R7:U_HDS_3:first": 895.0,
                    "B31R7:U_HDS_3:last20mean": 27.0,
                    "B31R7:U_HDS_4:first": 886.0,
                    "B31R7:U_HDS_4:last20mean": 29.0,
                },
                "result": {
                    "B31R7:U_HDS_1:first": True,
                    "B31R7:U_HDS_1:last20mean": True,
                    "B31R7:U_HDS_2:first": True,
                    "B31R7:U_HDS_2:last20mean": True,
                    "B31R7:U_HDS_3:first": True,
                    "B31R7:U_HDS_3:last20mean": True,
                    "B31R7:U_HDS_4:first": True,
                    "B31R7:U_HDS_4:last20mean": True,
                },
            }
        ),
        tau_u_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7:U_HDS_1:tau_charge": 0.084,
                    "B31R7:U_HDS_2:tau_charge": 0.083,
                    "B31R7:U_HDS_3:tau_charge": 0.082,
                    "B31R7:U_HDS_4:tau_charge": 0.084,
                    "B31R7:I_HDS_1:tau_charge": 0.074,
                    "B31R7:I_HDS_2:tau_charge": 0.074,
                    "B31R7:I_HDS_3:tau_charge": 0.073,
                    "B31R7:I_HDS_4:tau_charge": 0.075,
                },
                "act": {
                    "B31R7:U_HDS_1:tau_charge": 0.083,
                    "B31R7:U_HDS_2:tau_charge": 0.082,
                    "B31R7:U_HDS_3:tau_charge": 0.082,
                    "B31R7:U_HDS_4:tau_charge": 0.083,
                    "B31R7:I_HDS_1:tau_charge": 0.074,
                    "B31R7:I_HDS_2:tau_charge": 0.073,
                    "B31R7:I_HDS_3:tau_charge": 0.073,
                    "B31R7:I_HDS_4:tau_charge": 0.074,
                },
                "diff": {
                    "B31R7:U_HDS_1:tau_charge": 0.003,
                    "B31R7:U_HDS_2:tau_charge": 0.003,
                    "B31R7:U_HDS_3:tau_charge": 0.003,
                    "B31R7:U_HDS_4:tau_charge": 0.003,
                    "B31R7:I_HDS_1:tau_charge": 0.003,
                    "B31R7:I_HDS_2:tau_charge": 0.003,
                    "B31R7:I_HDS_3:tau_charge": 0.003,
                    "B31R7:I_HDS_4:tau_charge": 0.003,
                },
                "result": {
                    "B31R7:U_HDS_1:tau_charge": True,
                    "B31R7:U_HDS_2:tau_charge": True,
                    "B31R7:U_HDS_3:tau_charge": True,
                    "B31R7:U_HDS_4:tau_charge": True,
                    "B31R7:I_HDS_1:tau_charge": True,
                    "B31R7:I_HDS_2:tau_charge": True,
                    "B31R7:I_HDS_3:tau_charge": True,
                    "B31R7:I_HDS_4:tau_charge": True,
                },
            }
        ),
        first_r_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7:R_HDS_1:first20mean": 10.49,
                    "B31R7:R_HDS_2:first20mean": 10.57,
                    "B31R7:R_HDS_3:first20mean": 10.57,
                    "B31R7:R_HDS_4:first20mean": 10.47,
                },
                "act": {
                    "B31R7:R_HDS_1:first20mean": 10.48,
                    "B31R7:R_HDS_2:first20mean": 10.57,
                    "B31R7:R_HDS_3:first20mean": 10.57,
                    "B31R7:R_HDS_4:first20mean": 10.46,
                },
                "diff": {
                    "B31R7:R_HDS_1:first20mean": 0.5,
                    "B31R7:R_HDS_2:first20mean": 0.5,
                    "B31R7:R_HDS_3:first20mean": 0.5,
                    "B31R7:R_HDS_4:first20mean": 0.5,
                },
                "result": {
                    "B31R7:R_HDS_1:first20mean": True,
                    "B31R7:R_HDS_2:first20mean": True,
                    "B31R7:R_HDS_3:first20mean": True,
                    "B31R7:R_HDS_4:first20mean": True,
                },
            }
        ),
        capacitance_comp=pd.DataFrame(
            {
                "ref": {
                    "B31R7_C_HDS_1:capacitance": 0.008007626310772164,
                    "B31R7_C_HDS_2:capacitance": 0.007852412488174077,
                    "B31R7_C_HDS_3:capacitance": 0.007757805108798486,
                    "B31R7_C_HDS_4:capacitance": 0.008022922636103151,
                },
                "act": {
                    "B31R7_C_HDS_1:capacitance": 0.007919847328244274,
                    "B31R7_C_HDS_2:capacitance": 0.007757805108798486,
                    "B31R7_C_HDS_3:capacitance": 0.007757805108798486,
                    "B31R7_C_HDS_4:capacitance": 0.007934990439770554,
                },
                "diff": {
                    "B31R7_C_HDS_1:capacitance": nan,
                    "B31R7_C_HDS_2:capacitance": nan,
                    "B31R7_C_HDS_3:capacitance": nan,
                    "B31R7_C_HDS_4:capacitance": nan,
                },
                "result": {
                    "B31R7_C_HDS_1:capacitance": nan,
                    "B31R7_C_HDS_2:capacitance": nan,
                    "B31R7_C_HDS_3:capacitance": nan,
                    "B31R7_C_HDS_4:capacitance": nan,
                },
            }
        ),
    ),
]


def test_qh_ccc_output():
    # arrange
    analysis = analyses.QHCCCAnalysis("", 0, 1, False)
    analysis.voltage_events = voltage_events
    analysis.voltage_results = voltage_results
    analysis.voltage_current_events = voltage_current_events
    analysis.voltage_current_results = voltage_current_results

    # act
    output = quench_heater_ccc_output.get_output(analysis)

    # assert
    assert len(output) == 14


def test_qh_ccc_summary():
    # arrange
    analysis = analyses.QHCCCAnalysis("", 0, 1, False)
    analysis.voltage_events = voltage_events
    analysis.voltage_results = voltage_results
    analysis.voltage_current_events = voltage_current_events
    analysis.voltage_current_results = voltage_current_results

    # act
    output = quench_heater_ccc_output.get_summary(analysis)

    assert len(output) == 1
    assert all(isinstance(o, output_types.HTMLOutput) for o in output)


def test_qh_voltage_output():
    # arrange
    analysis = analyses.QuenchHeaterVoltageAnalysis("", "RQ10.L1", 900, 0, 1)
    analysis.voltage_events = voltage_events
    analysis.voltage_results = voltage_results

    # act
    output = quench_heater_voltage_output.get_output(analysis)

    # assert
    assert len(output) == 6


def test_qh_voltage_summary():
    # arrange
    analysis = analyses.QuenchHeaterVoltageAnalysis("", "RQ10.L1", 900, 0, 1)
    analysis.voltage_events = voltage_events
    analysis.voltage_results = voltage_results

    # act
    output = quench_heater_voltage_output.get_summary(analysis)

    # assert
    assert len(output) == 2
    assert all(isinstance(o, output_types.HTMLOutput) for o in output)


def test_qh_voltage_current_output():
    # arrange
    analysis = analyses.QuenchHeaterVoltageCurrentAnalysis("", "RB.A12", 900, 0, 1)
    analysis.voltage_current_events = voltage_current_events
    analysis.voltage_current_results = voltage_current_results

    # act
    output = quench_heater_voltage_current_output.get_output(analysis)

    # assert
    assert len(output) == 13


@pytest.mark.parametrize(
    ("voltage_current_events", "voltage_current_results"),
    [
        (([], [])),
        (voltage_current_events, voltage_current_results),
        (
            [
                commons.VoltageCurrentEvent(
                    "C30R7", GenericCircuitType.RB, "RB.A78", MagicMock(timestamp=0), MagicMock(timestamp=0)
                )
            ],
            [
                commons.VoltageCurrentResult(
                    "C30R7",
                    0,
                    pd.DataFrame(columns=["result"]),
                    pd.DataFrame(columns=["result"]),
                    pd.DataFrame(columns=["result"]),
                    pd.DataFrame(columns=["result"]),
                )
            ],
        ),
    ],
)
def test_qh_voltage_current_summary(
    voltage_current_events: list[commons.VoltageCurrentEvent],
    voltage_current_results: list[commons.VoltageCurrentResult],
):
    # arrange
    analysis = analyses.QuenchHeaterVoltageCurrentAnalysis("", "RB.A12", 900, 0, 1)
    analysis.voltage_current_events = voltage_current_events
    analysis.voltage_current_results = voltage_current_results

    # act
    output = quench_heater_voltage_current_output.get_summary(analysis)

    # assert
    assert len(output) == 2
    assert all(isinstance(o, output_types.HTMLOutput) for o in output)
