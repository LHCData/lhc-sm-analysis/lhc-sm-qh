import numpy as np
import pandas as pd
import pytest

from lhcsmqh.analyses import commons


def test_fill_value_at_location_0_with_latest_preceding_value_at_0():
    # arrange
    df = pd.DataFrame({"column": [10, 10, 9, 8, 5, 2]}, index=[-2, -1, 0, 1, 2, 3])

    # act
    result = commons.fill_value_at_location_0_with_latest_preceding(df)

    # assert
    assert result.iloc[:, 0].equals(df.iloc[:, 0])


def test_fill_value_at_location_0_with_latest_preceding_missing_value_at_0():
    # arrange
    df = pd.DataFrame({"column": [10, 10, 8, 5, 2]}, index=[-2, -1, 1, 2, 3])

    # act
    result = commons.fill_value_at_location_0_with_latest_preceding(df)

    # assert
    assert result.iloc[:, 0].equals(pd.Series(data=[10, 10, 10, 8, 5, 2], index=[-2, -1, 0, 1, 2, 3], name="column"))


def test_fill_value_at_location_0_with_latest_preceding_no_values_before_0():
    # arrange
    df = pd.DataFrame({"column": [10, 8, 5, 2]}, index=[1, 2, 3, 4])

    # act
    result = commons.fill_value_at_location_0_with_latest_preceding(df)

    # assert
    assert result.iloc[:, 0].equals(df.iloc[:, 0])


def test_fill_value_at_location_0_with_latest_preceding_no_values():
    # arrange
    df = pd.DataFrame({"column": []})

    # act
    result = commons.fill_value_at_location_0_with_latest_preceding(df)

    # assert
    assert result.empty


@pytest.mark.parametrize(
    ("u_values", "i_values", "expected_r_values"),
    [([10, 20, 30], [2, 4, 6], [5, 5, 5]), ([15, 25, 35], [3, 5, 7], [5, 5, 5])],
)
def test_calculate_resistance_basic(u_values, i_values, expected_r_values):
    # Arrange
    u_hds = [pd.DataFrame({"Circuit:U_HDS_1": u_values}, dtype=float)]
    i_hds = [pd.DataFrame({"Circuit:I_HDS_1": i_values}, dtype=float)]

    # act
    result = commons.calculate_resistance(u_hds, i_hds)

    # assert
    expected = pd.DataFrame({"Circuit:R_HDS_1": expected_r_values}, dtype=float)
    pd.testing.assert_frame_equal(result[0], expected)


@pytest.mark.parametrize(
    ("u_values", "i_values", "expected_r_values"),
    [([10, 20], [0, 4], [np.inf, 5]), ([10, 20], [0, 0], [np.inf, np.inf]), ([10, -20], [2, -4], [5, 5])],
)
def test_calculate_resistance_edge_cases(u_values, i_values, expected_r_values):
    # arrange
    u_hds = [pd.DataFrame({"Circuit:U_HDS_1": u_values}, dtype=float)]
    i_hds = [pd.DataFrame({"Circuit:I_HDS_1": i_values}, dtype=float)]

    # act
    result = commons.calculate_resistance(u_hds, i_hds)

    # assert
    expected = pd.DataFrame({"Circuit:R_HDS_1": expected_r_values}, dtype=float)
    pd.testing.assert_frame_equal(result[0], expected)


@pytest.mark.parametrize(
    ("tau", "resistance", "expected"),
    [
        (
            {"MQXFA.A2R1:U_HDS_1:tau_charge": [0.5], "MQXFA.A2R1:U_HDS_2:tau_charge": [0.6]},
            {"MQXFA.A2R1:R_HDS_1:first20mean": [0.1], "MQXFA.A2R1:R_HDS_2:first20mean": [0.2]},
            {"MQXFA.A2R1_C_HDS_1:capacitance": [5.0], "MQXFA.A2R1_C_HDS_2:capacitance": [3.0]},
        ),
        (
            {"MQXFA.A2R1:U_HDS_1:tau_charge": [0.5], "MQXFA.A2R1:U_HDS_2:tau_charge": [0.6]},
            {"MQXFA.A2R1:R_HDS_1:first20mean": [0.0], "MQXFA.A2R1:R_HDS_2:first20mean": [0.2]},
            {"MQXFA.A2R1_C_HDS_1:capacitance": [np.inf], "MQXFA.A2R1_C_HDS_2:capacitance": [3.0]},
        ),
    ],
)
def test_calculate_capacitance_multiple(tau, resistance, expected):
    # Arrange
    tau = pd.DataFrame(tau)
    resistance = pd.DataFrame(resistance)
    expected_capacitance = pd.DataFrame(expected)

    # Act
    result = commons.calculate_capacitance(tau, resistance)

    # Assert
    pd.testing.assert_frame_equal(result, expected_capacitance)


@pytest.mark.parametrize(("tau", "resistance", "expected_capacitance"), [([0.5], [0.1], [5.0]), ([0.4], [0.2], [2.0])])
def test_calculate_capacitance_basic(tau, resistance, expected_capacitance):
    # Arrange
    tau = pd.DataFrame({"MQXFA.A2R1:U_HDS_1:tau_charge": tau})
    resistance = pd.DataFrame({"MQXFA.A2R1:R_HDS_1:first20mean": resistance})
    expected_capacitance = pd.DataFrame({"MQXFA.A2R1_C_HDS_1:capacitance": expected_capacitance})

    # Act
    result = commons.calculate_capacitance(tau, resistance)

    # Assert
    pd.testing.assert_frame_equal(result, expected_capacitance)


@pytest.mark.parametrize(
    ("tau", "resistance", "expected_capacitance"),
    [([0.5], [0.0], [np.inf]), ([0.0], [0.1], [0.0]), ([-0.5], [-0.1], [5.0])],
)
def test_calculate_capacitance_edge_cases(tau, resistance, expected_capacitance):
    # Arrange
    tau = pd.DataFrame({"MQXFA.A2R1:U_HDS_1:tau_charge": tau})
    resistance = pd.DataFrame({"MQXFA.A2R1:R_HDS_1:first20mean": resistance})
    expected_capacitance = pd.DataFrame({"MQXFA.A2R1_C_HDS_1:capacitance": expected_capacitance})

    # Act
    result = commons.calculate_capacitance(tau, resistance)

    # Assert
    pd.testing.assert_frame_equal(result, expected_capacitance)
