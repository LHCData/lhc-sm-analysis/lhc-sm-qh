# SIGMON QH Analysis

This package contains classes for the analysis of Quench Heater discharges in RB, RQ, IPD, IPQ and IT circuits.  
They extract the decays from `QPS`' `U_HDS` and `I_HDS` signals (the latter is measured for RBs and RQs only) and compare them with the reference ones.

[MP3 TWiki on QH](https://twiki.cern.ch/twiki/bin/view/MP3/QHanalysisIST)

### Criteria:
- all characteristic times of the pseudo-exponential voltage (and current if available) decays calculated with the 'charge' approach are close enough to the reference ones:
  - for discharge level 900V:
    - +/- 3 ms for RB/RQ
    - +/- 5 ms for IPD/IPQ/IT
  - for discharge level 300V
    - +/- 20 ms for RB/RQ
    - +/- 10 ms for IPD/IT
    - +/- 20 ms for IPQ
- all initial voltages are within acceptance range:
  - for discharge level 900V:
    - between 780 and 980 V for RB
    - between 810 and 1020 V for RQ
    - between 810 and 1000 V for IPD/IPQ/IT
  - for discharge level 300V:
    - between 250 and 350 V for all circuits
- all final voltages are within acceptance range:
  - for discharge level 900V:
    - between 15 and 85 V for RB
    - between 20 and 45 V for RQ
    - between 0 and 20 V for IPD/IPQ/IT
  - for discharge level 300V:
    - between 0 and 10 V for all circuits
  - [RB and RQ] all initial resistances are +/- 0.5 Ohm from the reference ones;

> :warning: All reference discharges were done for 900V and the features of those discharges are used as the comparison base
for the discharges on all levels.

## QH CCC Analysis

This class is contained inside the [quench_header_ccc.py](/qh/lhcsmapi/api/analysis/qh/quench_heater_ccc.py) file. It is used by CCC operators to run a QH Analysis for every circuit type. 

### Inputs

| Name                   | Type(s)                                                        | Description                                                                                                                           |
|------------------------|----------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| `start_time`           | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Start time for the QH events query from PM                                                                                            |
| `stop_time`            | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Stop time for the QH events query from PM                                                                                             |
| `initial_charge_check` | `bool`                                                         | If it is set to `True`, the `max_charge` value will be queried from NxCALS and only events with `max_charge >= 800` will be analyzed. |

### Results

| Name                      | Type(s)                      | Description                                                                                                                                      |
|---------------------------|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| `low_charge_events`       | `pd.DataFrame`               | If `initial_charge_check == True` the dataframe will contain the list of events with `max_charge < 800`.                                         |
| `voltage_current_results` | `List[VoltageCurrentResult]` | A list of `VoltageCurrentResult` objects. The length of the list is equal to the number of analyzed events for both `RB` and `RQ` circuit types. |
| `voltage_results`         | `List[VoltageResult]`        | A list of `VoltageResult` objects. The length of the list is equal to the number of analyzed events for `IPQ`, `IPD` and `IT` circuit types.     |
| `voltage_current_events`  | `List[VoltageCurrentEvent]`  | A list of `VoltageCurrentEvent` objects. The length of the list is equal to the number of analyzed events for both `RB` and `RQ` circuit types.  |
| `voltage_events`          | `List[VoltageEvent]`         | A list of `VoltageEvent` objects. The length of the list is equal to the number of analyzed events for `IPQ`, `IPD` and `IT` circuit types.      |
| `events_number`           | `int`                        | Number of analyzed events.                                                                                                                       |

The classes are defined and described in [_common.py](/qh/lhcsmapi/api/analysis/qh/_common.py) file.

### Analysis output

The analysis output is a boolean value that comes from an `and` operation between all the bool values for every analyzed event.


## QH Voltage Current Analysis (for RB/RQ)

This class is contained inside the [quench_heater_voltage_current_analysis.py](/qh/lhcsmapi/api/analysis/qh/quench_heater_voltage_current_analysis.py) file.

### Inputs

| Name              | Type(s)                                                        | Description                                                   |
|-------------------|----------------------------------------------------------------|---------------------------------------------------------------|
| `circuit_type`    | `str`                                                          | A circuit type. Currently the analysis supports 'RB' and 'RQ' |
| `circuit_name`    | `str`                                                          | A name of a circuit. Must match the provided `circuit_type`   |
| `start_time`      | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Start time for the QH events query from PM                    |
| `discharge_level` | `int`                                                          | A nominal QH voltage (10, 300, 900)                           |
| `stop_time`       | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Stop time for the QH events query from PM                     |

### Results

| Name                      | Type(s)                      | Description                                                                                                                                      |
|---------------------------|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| `voltage_current_results` | `List[VoltageCurrentResult]` | A list of `VoltageCurrentResult` objects. The length of the list is equal to the number of analyzed events for both `RB` and `RQ` circuit types. |
| `voltage_current_events`  | `List[VoltageCurrentEvent]`  | A list of `VoltageCurrentEvent` objects. The length of the list is equal to the number of analyzed events for both `RB` and `RQ` circuit types.  |

The classes are defined and described in [_common.py](/qh/lhcsmapi/api/analysis/qh/_common.py) file.

### Analysis output

The analysis output is a boolean value that comes from an `and` operation between all the bool values for every analyzed event.  

## QH Voltage Analysis (for IPD/IPQ/IT)

This class is contained inside the [quench_heater_voltage_analysis.py](/qh/lhcsmapi/api/analysis/qh/quench_heater_voltage_analysis.py) file.

### Inputs

| Name              | Type(s)                                                        | Description                                                            |
|-------------------|----------------------------------------------------------------|------------------------------------------------------------------------|
| `circuit_type`    | `str`                                                          | A circuit type. Currently the analysis supports 'IPQ', 'IPD' and 'IT'  |
| `circuit_name`    | `str`                                                          | A name of a circuit. Must match the provided `circuit_type`            |
| `start_time`      | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Start time for the QH events query from PM                             |
| `discharge_level` | `int`                                                          | A nominal QH voltage (10, 300, 900)                                    |
| `stop_time`       | `int` (Unix timestamp), `str` (ISO8601) or `datetime.datetime` | Stop time for the QH events query from PM                              |

### Results

| Name                      | Type(s)                      | Description                                                                                                                                      |
|---------------------------|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| `voltage_results`         | `List[VoltageResult]`        | A list of `VoltageResult` objects. The length of the list is equal to the number of analyzed events for `IPQ`, `IPD` and `IT` circuit types.     |
| `voltage_events`          | `List[VoltageEvent]`         | A list of `VoltageEvent` objects. The length of the list is equal to the number of analyzed events for `IPQ`, `IPD` and `IT` circuit types.      |

The classes are defined and described in [_common.py](/qh/lhcsmapi/api/analysis/qh/_common.py) file.

### Analysis output

The analysis output is a boolean value that comes from an `and` operation between all the bool values for every analyzed event.
